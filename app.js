'use strict';

// Fichero configuración express

var express = require("express"),
  app = express(),
  bodyParser = require("body-parser"),
  methodOverride = require("method-override"),
  mongoose = require('mongoose');

  mongoose.Promise = require('bluebird');

var cors = require('cors');
var schedule = require('node-schedule');
var AdminCtrl = require('./controllers/admin');


var local = false;
// Connection to DB
if (local) {
  mongoose.connect('mongodb://localhost/swapp_db', function(err, res) {
    if (err) throw err;
    console.log('Connected to Local Database');
  });
} else {
  var uri = "mongodb://user:qdgxFFYii6khC2HP@swapptest-shard-00-00-ienga.gcp.mongodb.net:27017,swapptest-shard-00-01-ienga.gcp.mongodb.net:27017,swapptest-shard-00-02-ienga.gcp.mongodb.net:27017/swapp?ssl=true&replicaSet=SwappTest-shard-0&authSource=admin&retryWrites=true";
  mongoose.connect(uri, { useNewUrlParser: true  }, function(err, res) {
      //console.log('Connected to Remote Database');
    if (err) throw err;
    console.log('->->-> Connected to Remote Database <-<-<-');
  });
}



// Middlewares
app.use(bodyParser.urlencoded({  extended: false}));
app.use(bodyParser.json());
app.use(methodOverride());

/****	Cors 	***/
app.use((req, res, next) => {
    res.header('Access-Control-Allow-Origin', '*');
    res.header('Access-Control-Allow-Headers', 'Authorization, X-API-KEY, Origin, X-Requested-With, Content-Type, Accept, Access-Control-Allow-Request-Method');
    res.header('Access-Control-Allow-Methods', 'GET, POST, OPTIONS, PUT, DELETE');
    res.header('Allow', 'GET, POST, OPTIONS, PUT, DELETE');

    next();
});

//  Routes
var user_routes = require('./routes/user');
var offices_routes = require('./routes/office');
var games_routes = require('./routes/game');
var product_routes = require('./routes/product');
var order_routes = require('./routes/order');
var admin_routes = require('./routes/admin');
var mail_routes = require('./routes/notifications');
var genre_routes = require('./routes/genre');
var review_routes = require('./routes/review');
var webhook_routes = require('./routes/webhook');
var subscriptions_routes = require('./routes/subscriptions');
var migrations_routes = require('./routes/migrations');
var trophies_routes = require('./routes/trophy');
var notifications_routes = require('./routes/notifications');

//routes
app.use('/api', user_routes);
app.use('/api', offices_routes);
app.use('/api', games_routes);
app.use('/api', product_routes);
app.use('/api', order_routes);
app.use('/api', admin_routes);
app.use('/api', mail_routes);
app.use('/api', review_routes);
app.use('/api', genre_routes);
app.use('/api', webhook_routes);
app.use('/api', subscriptions_routes);
app.use('/api', migrations_routes);
app.use('/api', trophies_routes);
app.use('/api', notifications_routes);

module.exports = app;

var rule = new schedule.RecurrenceRule();
rule.dayOfWeek = [0, new schedule.Range(1, 6)];
//rule.hour = [0, new schedule.Range(1, 5)];
rule.minute = new schedule.Range(0, 59, 20);
schedule.scheduleJob(rule, function(){
  var req;
var res = [];
  AdminCtrl.getPendingOrdersMRW(req, res);
  console.log('Check status orders.');
});

const PORT = 8080;

// Start server
app.listen(PORT, function() {
  console.log("***Node server running on http://localhost:"+PORT+"***");
});
