var NotificationsCtrl = require('../controllers/notifications');
var SubscriptionsCtrl = require('../controllers/subscriptions');
var Subscription = require('../models/subscription');
var async = require('async');


function webhook(req, res) {

  // Retrieve the request's body and parse it as JSON:
  const event_json = req.body;

  if (event_json.type == 'customer.subscription.updated') {


    if (event_json.data.previous_attributes.cancel_at_period_end == false && event_json.data.object.cancel_at_period_end == true) {

      var req;
      req.params.id = event_json.data.object.id;
      req.body.status = 'cancel_at_period_end';
      SubscriptionsCtrl.updateSubscription(req, res);
      //Cancels at period end
    } else if (event_json.data.previous_attributes.cancel_at_period_end == true && event_json.data.object.cancel_at_period_end == false) {
      var req;
      req.params.id = event_json.data.object.id;
      req.body.status = 'active';
      SubscriptionsCtrl.updateSubscription(req, res);
      //Reactivate
    } else if (event_json.data.previous_attributes.status === 'trialing' && event_json.data.object.status === 'active') {
      //Trial to active
    } else if (event_json.data.previous_attributes.status === 'past_due' && event_json.data.object.status === 'active') {
      //Past due to active
      var req;
      req.params.id = event_json.data.object.id;
      req.body.status = event_json.data.object.status;
      SubscriptionsCtrl.updateSubscription(req, null);

    } else if ((event_json.data.previous_attributes.status === 'active' || event_json.data.previous_attributes.status === 'trialing') && event_json.data.object.status == 'past_due') {
      // Active to past due
      var req;
      req.params.id = event_json.data.object.id;
      req.body.status = event_json.data.object.status;
      SubscriptionsCtrl.updateSubscription(req, null);

    }
  } else if (event_json.type == 'customer.subscription.deleted') {
    if (event_json.data.object.status === 'canceled') {
      //Canceled
      //changeStatus($event_json['data']['object']['id'], 'canceled');
      var req;
      req.params.id = event_json.data.object.id;
      req.body.status = 'canceled';
      SubscriptionsCtrl.updateSubscription(req, null);

      //DELETE ALL QUEUE GAMES
    }
  } else if (event_json.type === 'customer.updated') {
    //Customer.updated
    if (event_json.data.previous_attributes.account_balance !== event_json.data.object.account_balance) {
      //Credit changed
      //setCredit((int) $event_json['data']['object']['description'], $event_json['data']['object']['account_balance'] / 100);
    }
  } else if (event_json.type === 'customer.subscription.created') {
    NotificationsCtrl.newSubscription(null, event_json.data.object.plan.id);
    res.send(200);
  } else if (event_json.type === 'charge.succeeded') {

    NotificationsCtrl.gameBougth(null, event_json.data.object.description + " " + event_json.data.object.amount / 100 + " €");
    res.send(200);
  }

  // Do something with event_json
  //
};


function getActiveSubscriptions(req, res) {
  // var stripe = require("stripe")("sk_live_guvFbTPzk0ZWZry6Io7JAddv");
  var sk_stripe_test = 'sk_test_E1SdK829cuZtucs5qI5dwQoU';
  var sk_stripe_live = 'sk_live_guvFbTPzk0ZWZry6Io7JAddv';

  var stripe = require("stripe")(sk_stripe_live);
  var nSubs = 0;

  stripe.subscriptions.list({
      limit: 100,
      status: 'trialing'
    },
    function(err, subscriptions) {
      if (err) res.status(500).send(err);

      async.forEach(subscriptions.data, function(item, callback) {
        //for (var i = 0; i < subscriptions.data.length; i++) {
        // if (subscriptions.data[i].status !== 'canceled') {

        //console.log(item);
        var subId;
        var billing_cycle_anchor;
        //if (item.status == 'active') {

        subId = item.id;
        var status = item.status;
        current_period_end = item.current_period_end;
        var date = new Date(current_period_end * 1000);
        if (date.getMonth() > 5) {
          console.log(subId + ' ' + item.status + ' Current period end: '  + date.getMonth() + '/' + date.getFullYear());
             nSubs++;
        }
        if (current_period_end == undefined) {
          res.status(200).send(item);
        }

        Subscription.findOne({
          "subscriptionId": subId
        }, function(err, sub) {
          if (err) return res.status(500).send(err);
          if (!sub) {
            // console.log(subId + ' Not found.');
          } else {


            console.log(current_period_end + new Date(current_period_end * 1000));

            var todaysDate = new Date();
            if (todaysDate > new Date(current_period_end * 1000)) {


            } else {
              //return res.status(500).send(subId + ' billing_cycle_anchor menor.');
            }
            sub.status = status;
            sub.current_period_end = new Date(current_period_end * 1000);
            sub.save(function(err, sub2) {
              if (err) return res.send(500, err.message);
              callback();
              // console.log(subId + ' Updated.');
            });


          }
        });

      }, function(err) {
        console.log('iterating done');
      });

      console.log('Num subscriptions ' + nSubs);

      //res.status(200).send(subscriptions);
      // asynchronously called
    });

}




module.exports = {
  webhook,
  getActiveSubscriptions
}
