var mongoose = require('mongoose');
var Genre = require('../models/genre');


/*** Metodo para guardar un nuevo género ***/
function saveGenre(req, res) {
  var genre = new Genre({
    name: req.body.name
  });

  genre.save(function(err, genre) {
    if (err) return res.send(500, err.message);
    res.status(200).jsonp(genre);
  });
};

/*** Metodo para ver los géneros disponibles ***/
function getGenres(req, res) {

  Genre.find()
    .exec(function(err, genres) {
      if (err) return res.send(500, err.message);
      if (!genres) {
        return res.send(404, 'El reviews no existe.');
      } else {
        res.status(200).jsonp(genres);
      }
    });
};

module.exports = {
saveGenre,
getGenres
}
