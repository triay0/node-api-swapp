var mongoose = require('mongoose');
var User = require('../models/user');
var Subscription = require('../models/subscription');
var QueueGame = require('../models/queueGame');
var Game = require('../models/game');
var Review = require('../models/review');
var NotificationsCtrl = require('../controllers/notifications');
var async = require('async');

var sk_stripe_test = 'sk_test_E1SdK829cuZtucs5qI5dwQoU';
var sk_stripe_live = 'sk_live_guvFbTPzk0ZWZry6Io7JAddv';

var stripe = require("stripe")(sk_stripe_live);
var moment = require('moment');
var admin = require('firebase-admin');
var firebase = require("firebase/app");
require("firebase/auth");
var config = {
  apiKey: "AIzaSyAkyCOgpb-1OiIm7uKaw1oA8GiVCQK-aas",
  authDomain: "swappgames-9b257.firebaseapp.com",
  databaseURL: "https://swappgames-9b257.firebaseio.com",
  projectId: "swappgames-9b257",
  storageBucket: "swappgames-9b257.appspot.com",
  messagingSenderId: "999884098832"
};
firebase.initializeApp(config);
// //GET - Return a User with specified ID
// function getUser(req, res) {
//   User.findById(req.params.id)
//     .populate({
//       path: 'subscriptions',
//       select: 'subscriptionId plan status createdAt'
//
//     }).exec(function(err, user) {
//       if (err) return res.status(500).send(err.message);
//       if (!user) res.status(404).jsonp('User not found.');
//
//       res.status(200).jsonp(user);
//     });
//
// };

/*** Metodo para Sacar Datos del Usuario ***/
function getUser(req, res) {
  // console.log('jy');
  User.aggregate([{
      $lookup: {
        from: "subscriptions",
        localField: "subscriptions",
        foreignField: "_id",
        as: "subs"
      }
    },
    {
      $match: {
        uid: req.user.uid
      }
    },
    {
      $project: {
        name: 1,
        nick: 1,
        phone: 1,
        address: 1,
        email_auth: 1,
        subscription: {
          $arrayElemAt: ["$subs", -1],
        },
      }
    },
    {
      $project: {
        name: 1,
        nick: 1,
        phone: 1,
        address: 1,
        email_auth: 1,
        "subscription.subscription_id": 1,
        "subscription.plan": 1,
        "subscription.status": 1,
        "subscription.current_period_end": 1

      }
    },
  ], function(err, user) {
    if (err) {
      return res.status(500).send(err.message);
    }
    if (!user[0]) {
      saveUser(req, res); //res.send(404, 'El usuario no existe.');
    } else {
      return res.status(200).jsonp(user[0]);
    }
  });
};


/*** Metodo para Sacar Datos del Usuario ***/
function getUserById(req, res) {
    User.aggregate([{
      $lookup: {
        from: "subscriptions",
        localField: "subscriptions",
        foreignField: "_id",
        as: "subs"
      }
    },
    {
      $match: {
        _id: mongoose.Types.ObjectId(req.params.id)
      }
    },
    {
      $project: {
        name: 1,
        nick: 1,
        phone: 1,
        address: 1,
        email_auth: 1,
        subscription: {
          $arrayElemAt: ["$subs", -1],
        },
      }
    },
    {
      $project: {
        name: 1,
        nick: 1,
        phone: 1,
        address: 1,
        email_auth: 1,
        "subscription.subscription_id": 1,
        "subscription.plan": 1,
        "subscription.status": 1,
        "subscription.current_period_end": 1

      }
    },
  ], function(err, user) {
    if (err) {
      return res.status(500).send(err.message);
    }
    if (!user[0]) {
      res.send(404, 'El usuario no existe.');
    } else {
      return res.status(200).jsonp(user[0]);
    }
  });
};


/*** Metodo para comporobar estado de la subscripcion y pedidos mensuales  ***/
function checkSubscription(req, res) {

  async.parallel({
    status: function(statusSubs) {
      User.aggregate([{
          $lookup: {
            from: "subscriptions",
            localField: "subscriptions",
            foreignField: "_id",
            as: "subs"
          }
        },
        {
          $match: {
            uid: req.user.uid
          }
        },
        {
          $project: {
            subscription: {
              $arrayElemAt: ["$subs", -1],
            },
          }
        },
        {
          $project: {
            "subscription._id": 1,
            "subscription.plan": 1,
            "subscription.status": 1,
            "subscription.current_period_end": 1

          }
        },
      ]).exec(statusSubs);
    },
    orders: function(orders) {
      User.find({
          uid: req.user.uid
        })
        .populate({
          path: 'orders',
          populate: {
            path: 'gameIn',
            select: 'name cover createdAt',
          }
        })
        .select('orders')
        .exec(orders);
    }
  }, function(err, result) {
    if (err) res.status(500).send(err);
    var numOrders = 0;
    var limitOrders = true;
    var pendingOrders = false;
    var resetOrders;
    var newTime = new Date();
    newTime.setMonth(newTime.getMonth() + 1);
    var plan;
    var status;

    if (result.status[0].subscription != undefined) {
      plan = result.status[0].subscription.plan;
      status = result.status[0].subscription.status;

      if (result.status[0].subscription.current_period_end > newTime) { // If +1 month
        result.status[0].subscription.current_period_end.setMonth(newTime.getMonth());
      }
      resetOrders = result.status[0].subscription.current_period_end;
      var startPeriod = new Date(resetOrders);
      startPeriod = startPeriod.setMonth(startPeriod.getMonth() - 1);
    }
    //**/*/*/*/*/*/ FALTA COMPROBAR POR FECHA
    for (var i = 0; i < result.orders[0].orders.length; i++) {
      if (result.orders[0].orders[i].statusFlutter >= 0 && result.orders[0].orders[i].statusFlutter < 6) {
        pendingOrders = true;
      }
      if (result.orders[0].orders[i].createdAt > startPeriod) {
        if (result.orders[0].orders[i].statusFlutter != -1) {

          numOrders++;
        }
      }
    }

    var maxOrders;
    if (plan != undefined) {
      if (plan.includes('basic')) {
        maxOrders = 2;
      } else {
        maxOrders = 3;
      }
    }

    if (numOrders >= maxOrders) {
      limitOrders = false;
    }

    var status = {
      'plan': plan,
      'status': status,
      'pendingOrders': pendingOrders,
      'freeOrder': limitOrders,
      'resetOrders': resetOrders
    }
    res.status(200).send(status);
  });
}

/*** Método para registrar un nuevo usuario ***/
function saveUser(req, res) {

  stripe.customers.create({
    email: req.user.email,
    description: req.user.uid,
  }, function(err, customer) {
    if (err) return res.status(500).send(err);
    console.log(customer);
    var user = new User({
      uid: req.user.uid,
      name: req.user.name,
      email_auth: req.user.email,
      stripe_id: customer.id,
      createdAt: moment().format()
    });

    user.save(function(err, user) {
      if (err) return res.status(500).send(err.message);
      res.status(200).jsonp(user);
    });
  });
};

/*** Método para actualizar datos de Usuario ***/
function updateUser(req, res) {
  User.findOne({
    uid: req.user.uid
  }, function(err, user) {
    user.name = req.body.name;
    user.nick = req.body.nick;
    user.phone = req.body.phone;
    user.address.postal1 = req.body.address.postal1;
    user.address.postal2 = req.body.address.postal2;
    user.address.zipcode = req.body.address.zipcode;
    user.address.province = req.body.address.province;
    user.address.city = req.body.address.city;


    user.save(function(err) {
      if (err) return res.send(500, err.message);
      res.status(200).jsonp(user);
    });
  });
};

/*** Método para buscar Usuario por nombre ***/
function findUser(req, res) {
  User.find({
      // name: {
      //   $regex: req.params.id
      // },
      email_auth: {
        $regex: req.params.id
      }
    }).limit(10)
    // {
    //    limit:10,
    //   // sort:{
    //   //       subscriptions: -1 //Sort by Date Added DESC
    //   //   }
    // }
    .exec(function(err, users) {
      res.status(200).jsonp(users);

    });
};

/*** Método para guardar tarjeta ***/
function saveCard(req, res) {
  var userid = req.user.sub;
  var params = req.body;

  User.findOne({
    uid: req.user.uid
  }, function(err, user) {
    if (err) return res.status(500).send(err.message);
    if (!user) {
      return res.status(404).send('El usuario no existe.');
    } else {
      stripe.customers.createSource(user.stripe_id, {
        source: params.token
      }, function(err, card) {
        if (err) return res.status(500).send(err.message);
        stripe.customers.update(user.stripe_id, {
          default_source: card.id
        });
        res.status(200).jsonp(card);
      });
    }
  });
}


/*** Método para borrar un juego de la cola ***/
function deleteQueue(req, res) {

  QueueGame.findOne({
    $and: [{
        game: req.params.id
      },
      {
        user: req.user._id
      },
      {
        requested: false
      },
      {
        deleted: false
      }
    ]
  }, function(err, qgame) {
    if (err) return res.send(500, err.message);

    if (!qgame) return res.send(404, 'QueueGame not found.');

    qgame.deleted = true;

    qgame.save(function(err) {
      if (err) return res.send(500, err.message);
      res.status(200).jsonp(qgame);
    });

  });

}

/*** Método para mostrar los cola del usuario ***/
function getQueue(req, res) {

  QueueGame.aggregate([{
        $lookup: {
          from: "games",
          localField: "game",
          foreignField: "_id",
          as: "game"
        }
      },
      {
        $lookup: {
          from: "queuegames",
          localField: "game.queue",
          foreignField: "_id",
          as: "queue"
        }
      },
      {
        $unwind: '$game'
      },
      {
        $match: {
          "user": req.user._id,
          "deleted": false,
          "requested": false,
        }
      },
      {
        "$project": {
          "game._id": 1,
          "game.name": 1,
          "game.cover": 1,
          "game.stock": 1,
          "game.booking": 1,
          "game.releaseDate": 1,

          "queue": {
            "$filter": {
              "input": "$queue",
              "as": "que",
              "cond": {
                $and: [{
                    $eq: ["$$que.deleted", false]
                  },
                  {
                    $eq: ["$$que.requested", false]
                  }
                ]
              }
            }
          },
        }
      },

    ]).exec()
    .then(queue => {

      for (var q = 0; q < queue.length; q++) {

        console.log(queue[q].game.name);
        console.log(queue[q].queue);
        queue[q].queue = queue[q].queue.sort(function(a, b) {

          return (new Date(a.createdAt) - new Date(b.createdAt));
        });

        console.log('user: ' + req.user._id);
        console.log(queue[q].game.name);

        console.log(queue[q].queue);

        queue[q].game.queue = queue[q].queue.findIndex(queue => queue.user == req.user._id) + queue[q].game.stock;
        queue[q].game.createdAt = queue[q].queue.findIndex(queue => queue.user == req.user._id).createdAt;


        console.log(queue[q].queue.findIndex(queue => queue.user == req.user._id));
        queue[q].queue = undefined;
      }
      res.status(200).jsonp(queue);

    });
}

/*** Método para mostrar los juegos que tiene en casa el usuario ***/
function getGamesAtHome(req, res) {
  User.findOne({
      uid: req.user.uid
    })
    .populate('games', 'name cover'
      //  //   //populate: {  path: 'gameIn', select: 'game',  populate: { path: 'game', select: 'name cover' }}
      //  //
    )
    .exec(function(err, user) {
      if (err) return res.send(500, err.message);
      if (!user) {
        return res.send(404, 'El usuario no existe.');
      } else {
        return res.status(200).jsonp(user.games);
      }
    });
}

/*** Método para mostrar los pedidos del usuario ***/
function getOrders(req, res) {
  User.findOne({
      uid: req.user.uid

    })
    .populate({
      path: 'orders',
      match: {
        'statusFlutter': {
          $gte: 0
        }
      },
      populate: {
        path: 'gameIn gameOut',
        select: 'name cover',

      }


    })
    .select('orders')
    .exec(function(err, user) {
      if (err) return res.send(500, err.message);
      if (!user) {
        return res.send(404, 'El usuario no existe.');
      } else {


        return res.status(200).jsonp(user.orders);
      }
    });
}

/*** Método para saber si el usuario tiene una tarjeta ***/
function hasCard(req, res) {
  User.findOne({
    uid: req.user.uid
  }, function(err, user) {
    if (err) return res.send(500, err.message);
    if (!user) {
      return res.send(404, 'El usuario no existe.');
    } else {
      stripe.customers.retrieve(
        user.stripe_id,
        function(err, customer) {
          if (err) return res.send(500, err.message);
          var card = false;
          if (customer.sources.total_count > 0) {
            card = true;
          }
          return res.status(200).send(card);

        }
      );
    }
  });

}

/*** Método para crear token a partir del usuario ***/
function createToken(req, res) {

  var uid = req.body.uid;
  if (!uid) {
    return res.status(400).jsonp("Uid required");
  }

  admin.auth().createCustomToken(uid)
    .then(function(customToken) {
      var user = firebase.auth().signInWithCustomToken(customToken)
        .then(function(user) {
          firebase.auth().currentUser.getIdToken( /* forceRefresh */ true).then(function(idToken) {
            // Send token to your backend via HTTPS

            res.status(200).send(idToken);
            // ...
          }).catch(function(error) {
            // Handle error
          });
        }).catch(function(error) {
          // Handle Errors here.
          var errorCode = error.code;
          var errorMessage = error.message;
          console.log(user);



          // ...
        });

    })
    .catch(function(error) {
      console.log("Error creating custom token:", error);
    });
}


module.exports = {
  getUser,
  saveUser,
  findUser,
  updateUser,
  saveCard,
  hasCard,
  deleteQueue,
  getQueue,
  getOrders,
  createToken,
  getGamesAtHome,
  getUserById,
  checkSubscription,

}
