'use strict'
var mongoose = require('mongoose');
var Order = require('../models/order');
var User = require('../models/user');
var QueueGame = require('../models/queueGame');
var OfficeCtrl = require('../controllers/office');
var NotificationsCtrl = require('../controllers/notifications');
var soap = require('soap');
const fs = require('fs');

function getPendingOrders(req, res) {
  Order.find({

      status: {
        "$gt": 0
      }

    })
    .populate({

      path: 'gameIn',
      select: 'name cover',
      populate: {
        path: 'game',
        select: 'id name cover'
      },

      //select: 'name cover'
    })
    .populate({
      path: 'gameOut',
      select: 'id name cover'
    })
    .exec(function(err, orders) {
      if (err) return res.send(500, err.message);
      if (!orders) {
        return res.send(404, 'El usuario no existe.');
      } else {
        return res.status(200).jsonp(orders);
      }
    });
};

async function getPendingOrdersMRW(req, res) {
  let response = res;
  console.log("REsponse " + res);

  var orders = await Order.find({
      $and: [{
          statusFlutter: {
            "$gt": -1
          }
        },
        {
          statusFlutter: {
            "$lt": 6
          }
        },
        {
          trackingIn: {
            $regex: "^04320"
          }
        }
      ]
    })
    .select('name trackingIn statusFlutter gameOut user');
  // Vaciamos fichero actualización estado pedidos
  fs.truncate('./tmp/updateMRW.txt', 0, function() {})
  // Recorrer todos los pedidos y comprobar estado
  for await (const order of orders) {
    var req = [];
    req.params = [];
    if (order.gameOut.length > 0) {
      req.params.out = true;
    } else {
      req.params.out = false;
    }
    req.params.tracking = order.trackingIn;
    req.params.status = order.statusFlutter;
    req.params._id = order._id;
    req.params.user = order.user;
    var result = await OfficeCtrl.getTrackingMRW(req, res);
  }

  fs.readFile('./tmp/updateMRW.txt', 'utf8', function(err, contents) {
    if (err) console.log(err);
    if (contents.length != 0) {
      NotificationsCtrl.newAlert(contents, res);
      console.log('Pedidos actualizados :)');
    } else {
      console.log('Pedidos actualizados :)');
    }
  });
};

function searchUser(req, res) {
  User.find({
    name: {
      '$regex': req.params.id
    }
  }, function(err, users) {
    if (err) res.send(500, err.message);

    res.status(200).jsonp(users);
  });

};

function getOrders(req, res) {
  User.findById(
      req.params.id
    )
    .populate({
      path: 'orders',
      populate: {
        path: 'gameIn',
        select: 'name cover',

      },


    })
    .populate({
      path: 'orders',
      populate: {
        path: 'gameOut',
        select: 'name cover',

      },


    })
    .exec(function(err, user) {
      if (err) return res.send(500, err.message);
      if (!user) {
        return res.send(404, 'El usuario no existe.');
      } else {
        return res.status(200).jsonp(user.orders);
      }
    });
};

function getHome(req, res) {
  User.find({
    name: {
      '$regex': req.params.id
    }
  }, function(err, users) {
    if (err) res.send(500, err.message);

    res.status(200).jsonp(users);
  });

};

function getQueue(req, res) {

  QueueGame.find({
      user: req.params.id,
      deleted: {
        $eq: false
      },
      requested: {
        $eq: false
      }
    })

    .populate('game', 'name cover')

    .exec(function(err, queue) {
      if (err) res.send(500, err.message);
      res.status(200).jsonp(queue);

    });
};


function getQueue(req, res) {

  QueueGame.find({
      user: req.params.id,
      deleted: {
        $eq: false
      },
      requested: {
        $eq: false
      }
    })

    .populate('game', 'name cover')

    .exec(function(err, queue) {
      if (err) res.send(500, err.message);
      res.status(200).jsonp(queue);

    });
};

function updateUser(req, res) {
  User.findById(
    req.params.id
  ).exec(function(err, user) {
    user.name = req.body.name;
    user.nick = req.body.nick;
    user.phone = req.body.phone;
    user.address.postal1 = req.body.postal1;
    user.address.postal2 = req.body.postal2;
    user.address.zipcode = req.body.zipcode;
    user.address.province = req.body.province;
    user.address.city = req.body.city;

    user.save(function(err) {
      if (err) return res.send(500, err.message);
      res.status(200).jsonp(user);
    });
  });
};


module.exports = {
  getPendingOrders,
  searchUser,
  getOrders,
  getHome,
  getQueue,
  updateUser,
  getPendingOrdersMRW
}
