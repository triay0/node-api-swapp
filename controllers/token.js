var Token = require('../models/token');
var User = require('../models/user');

/*** Método para guardar un token FCM ***/
function saveToken(req, res) {
  var userid = req.user.uid;

  var token = new Token({
    token: req.body.token,
    platform: req.body.platform,
    user: req.user._id,
    createdAt: moment().format(),
    valid: true,
  });

  token.save(function(err) {
    if (err) return res.send(500, err);
    User.findOne({
        uid: userid
      })
      .exec(function(err, user) {
        if (err) return handleError(err);
        if (!user) return res.send(404, 'User not found.');

        user.fcmTokens.push(token);
        user.save(function(err) {
          if (err) return res.send(500, err);
          res.status(200).jsonp(user);
        });
      });
  });
}

/*** Método para borrar un token FCM(cuando cierra sesión) ***/
function invalidateToken(req, res) {

  var params = req.body;
  console.log('token:  '+params.token);

  Token.findOne({
    token: params.token
  }, function(err, token) {
    if (err) return res.send(500, err.message);
    if (token) {
      token.valid = false;

      token.save(function(err) {
        if (err) return res.send(500, err.message);
        res.status(200).jsonp(token);
      });
    } else {
      return res.status(200);
    }
  });
}

/*** Método para mostrar los token FCM(válidos) del usuario ***/
function getTokenFCM(req, res) {
  User.findOne({
      uid: req.user.uid
    })
    .populate({
      path: 'fcmTokens',
      match: {
        valid: {
          $eq: true
        }
      }
    })
    .exec(function(err, user) {
      if (err) return res.send(500, err.message);
      if (!user) {
        return res.send(404, 'El usuario no existe.');
      } else {
        return res.status(200).jsonp(user.fcmTokens);
      }
    });
}


module.exports ={
  addtoken,
  invalidateToken,
  getTokenFCM
}
