var mongoose = require('mongoose');
var Trophy = require('../models/trophy');

function saveTrophy(req, res) {
    var trophy = new Trophy({
      title: req.body.title,
      icon: req.body.icon
    });

    trophy.save(function(err, trophy) {
      if (err) return res.send(500, err.message);
      res.status(200).jsonp(trophy);
    });
};

function getTrophies(req, res) {
  Trophy.find()
    .exec(function(err, trophies) {
      if (err) return res.send(500, err.message);
      if (!trophies) {
        return res.send(404, 'No hay trofeos.');
      } else {
        res.status(200).jsonp(trophies);
      }
    });
};

module.exports = {
  saveTrophy,
  getTrophies
}
