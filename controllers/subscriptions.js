var Subscription = require('../models/subscription');
var User = require('../models/user');
var mongoose = require('mongoose');
var moment = require('moment');
var NotificationsCtrl = require('../controllers/notifications');

var sk_stripe_test = 'sk_test_E1SdK829cuZtucs5qI5dwQoU';
var sk_stripe_live = 'sk_live_guvFbTPzk0ZWZry6Io7JAddv';

var stripe = require("stripe")(sk_stripe_live);
// exports.getProducts = function(req, res) {
//   var stripe = require("stripe")("sk_live_guvFbTPzk0ZWZry6Io7JAddv");
//
//     stripe.products.list(
//   { active: true, type: 'good'},
//   function(err, products) {
//     // asynchronously called
//     console.log(products.data);
//   }
// );
//
//
//
//   };
function createSubscription(req, res) {
  var params = req.body;

  User.findOne({
    uid: req.user.uid
  }, function(err, user) {

    stripe.customers.update(
      user.stripe_id, { //Añadir el source
        source: params.token
      },
      function(err, customer) {
        if (err)
          return res.status(err.statusCode).send(err.message);

        stripe.subscriptions.create({ // Crear subscription
          customer: user.stripe_id,
          items: [{
            plan: params.plan,
          }, ]
        }, function(err, subscription) {

          if (err) return res.status(err.statusCode).send(err.message);

          // asynchronously called
          var subscription2 = new Subscription({
            subscription_id: subscription.id, //Subs.id,
            plan: params.plan,
            status: subscription.status,
            user: user._id,
            platform: params.platform,
            created_at: moment().format(),
            current_period_end: new Date(subscription.current_period_end * 1000)
          });

          subscription2.save(function(err) {
            if (err) return res.status(500).send(err);
            user.subscriptions.push(subscription2);
            user.save(function(err) {
              if (err) return res.status(500).send(err);
              var message = "NEW SUBSCRIBER " + params.plan;
              NotificationsCtrl.newAlert(message, res);
              res.status(200).jsonp(user);
            });
          });
        });
      }
    );

  });
}

function cancelSubscription(req, res) {
  var params = req.body;

  User.aggregate([{
      $lookup: {
        from: "subscriptions",
        localField: "subscriptions",
        foreignField: "_id",
        as: "subs"
      }
    },
    {
      $match: {
        uid: req.user.uid
      }
    },
    {
      $project: {
        subscription: {
          $arrayElemAt: ["$subs", -1],
        },
      }
    },
    {
      $project: {
        "subscription.subscription_id": 1,
        "subscription.plan": 1,
        "subscription.status": 1,
        "subscription.current_period_end": 1

      }
    },
  ], function(err, user) {
    if(err) return res.status(500).send(err);

    stripe.subscriptions.update(
      user[0].subscription.subscription_id, {
        cancel_at_period_end: true
      },
      function(err, subscription) {
        if (err) return res.send(500, err);
        Subscription.findOne({
          subscriptionId: user[0].subscription.subscriptionId
        }, function(err, subs) {
          if (err) return res.send(500, err.message);
          if (!subs) return res.send(404, 'Subscription not found.');
          console.log(user[0].subscription.subscriptionId);
          subs.canceledAt = moment().format();
          subs.updatedAt = moment().format();
          subs.date_return_games = params.date_return_games;
          subs.status = 'pending_canceled';

          subs.save(function(err, sub) {
            if (err) return res.send(500, err.message);
            var message = "Canceled " + params.date_return_games + user.uid;
            NotificationsCtrl.newAlert(message, res);
            res.status(200).jsonp(sub);
          });
        });
      }
    );

  });


}

function cancelReason(req, res) {
  var params = req.body;

  User.aggregate([{
      $lookup: {
        from: "subscriptions",
        localField: "subscriptions",
        foreignField: "_id",
        as: "subs"
      }
    },
    {
      $match: {
        uid: req.user.uid
      }
    },
    {
      $project: {
        subscription: {
          $arrayElemAt: ["$subs", -1],
        },
      }
    },
    {
      $project: {
        "subscription.subscriptionId": 1,
        "subscription.plan": 1,
        "subscription.status": 1,
        "subscription.current_period_end": 1

      }
    },
  ], function(err, user) {

    Subscription.findOne({
      subscriptionId: user[0].subscription.subscriptionId
    }, function(err, subs) {
      if (err) return res.send(500, err.message);
      if (!subs) return res.send(404, 'Subscription not found.');
      subs.reason = params.reason;
      subs.comment = params.comment;
      subs.save(function(err, sub) {
        if (err) return res.send(500, err.message);
        res.status(200).jsonp(sub);
      });
    });
  });

}

function reactivateSubscription(req, res) {
  var params = req.body;

  User.aggregate([{
      $lookup: {
        from: "subscriptions",
        localField: "subscriptions",
        foreignField: "_id",
        as: "subs"
      }
    },
    {
      $match: {
        uid: req.user.uid
      }
    },
    {
      $project: {
        subscription: {
          $arrayElemAt: ["$subs", -1],
        },
      }
    },
    {
      $project: {
        "subscription.subscriptionId": 1,
        "subscription.plan": 1,
        "subscription.status": 1,
        "subscription.current_period_end": 1

      }
    },
  ], function(err, user) {
    stripe.subscriptions.update(
      user[0].subscription.subscriptionId, {
        cancel_at_period_end: false
      },
      function(err, subs) {
        if (err) return res.send(500, err);

        Subscription.findOne({
          subscriptionId: user[0].subscription.subscriptionId
        }, function(err, subscription) {

          subscription.canceledAt = null;
          subscription.updatedAt = moment().format();
          subscription.status = subs.status;
          subscription.reason = null;
          subscription.comment = null;
          subscription.date_return_games = null;
          subscription.save(function(err) {
            if (err) return res.send(500, err.message);
            res.status(200).jsonp(subscription);
          });
        });
      }
    );
  });
}

function activateSubscription(req, res) {
  var params = req.body;
  stripe.subscriptions.update(
    params.subscription, {
      trial_end: 'now'
    },
    function(err, subs) {
      if (err) return res.send(500, err);

      Subscription.findOne({
        subscription: params.subscription
      }, function(err, subscription) {
        subscription.updated_at = moment().format(),
          subscription.status = subs.status,

          subscription.save(function(err) {
            if (err) return res.send(500, err.message);
            res.status(200).jsonp(subscription);
          });
      });
    }
  );
}

function updateSubscription(req, res) {
  Subscription.findOne({
    subscription: req.params.id
  }).exec(function(err, subscriptionResult) {
    if (err) res.send(500, err.message);
    if (!subscriptionResult) res.send(404, 'No subscription found.');
    subscriptionResult.status = req.body.status;


    subscriptionResult.save(function(err) {
      if (err) return res.send(500, err.message);
      res.status(200).jsonp(subscriptionResult);
    });
  });
};

module.exports = {
  createSubscription,
  cancelSubscription,
  reactivateSubscription,
  activateSubscription,
  updateSubscription,
  cancelReason
};
