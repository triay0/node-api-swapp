var mongoose = require('mongoose');

var QueueGame = require('../models/queueGame');
var Game = require('../models/game');
var User = require('../models/user');
var Order = require('../models/order');
var Token = require('../models/token');
var Subscription = require('../models/subscription');
var moment = require('moment');
var async = require('async');


function migrateGames(req, res) {
  var action = [];
  var adventure = [];
  var racing = [];
  var sports = [];
  var strategy = [];
  var role = [];
  var others = [];

  Game.find({
      relased: 1
    }).stream()
    .on('data', function(doc) {
      //console.log(doc.name);

      Game.findById(doc._id, function(err, game) {
        if (err) return res.send(500, "Error getting game: " + err + game);
        if (!game) return res.send(404, "Game not found.");


        game.genres = [];
        switch (doc.genre2) {
          case "1":
            action.push(doc.name);

            game.genres.push(mongoose.Types.ObjectId("5c3e0ef57f4d430ca4b02186"));

            break;
          case "2":
            adventure.push(doc.name);
            game.genres.push(mongoose.Types.ObjectId("5c3e0f017f4d430ca4b02187"));
            break;
          case "3":
            racing.push(doc.name);
            game.genres.push(mongoose.Types.ObjectId("5c3e10077f4d430ca4b0218a"));

            break;
          case "4":
            sports.push(doc.name);
            game.genres.push(mongoose.Types.ObjectId("5c3e0fcd7f4d430ca4b02189"));
            break;
          case "5":
            strategy.push(doc.name);
            //game.genres.push(mongoose.Types.ObjectId("5c3e10077f4d430ca4b0218a"));
            break;
          case "6":
            role.push(doc.name);
            game.genres.push(mongoose.Types.ObjectId("5c4ae50020040c0011d6e56d"));
            break;
          default:
            others.push(doc.name);
            break;
        }

        game.save(function(err, game) {
          if (err) return res.send(500, "Error saving game: " + err + game);
          if (!game) return res.send(404, "Game not found.");
        });
      });




    })
    .on('error', function(err) {
      // handle error
    })
    .on('end', function() {
      console.log(action);
      return res.send(200, "action" + JSON.stringify(others));

    });
}


function dropGamesQueue(req, res) {
  //db.collection.updateMany({}, {$unset:{"queue":1}})
  //db.getCollection('games').update({}, {$unset: {queue:1}}, false, true);

  QueueGame.collection.drop();



}


function migrateQueue(req, res) {

  QueueGame.find({}).stream()
    .on('data', function(doc) {


      //game_id to ref_game
      //push queue to games
      // if (doc.game_id != undefined) {
      //   Game.findOne({
      //       id: doc.game_id
      //     })
      //     .exec(function(err, game) {
      //       if (err) return res.send(500, err);
      //       if (!game) {
      //         console.log('Game not found: ' + doc.game_id);
      //         // return res.send(404, 'Game not found: ' + doc.game_id);
      //       } else {
      //         doc.game = mongoose.Types.ObjectId(game._id);
      //
      //         doc.save(function(err) {
      //           if (err) return res.send(500, err.message + doc.id);
      //           if (game['queue'] == undefined) {
      //             game.queue = [];
      //           }
      //
      //           if (game['genre2'] == undefined) {
      //             game.genre2 = [];
      //           }
      //           game.queue.push(doc._id);
      //           delete game.__v;
      //
      //
      //           game.save(function(err) {
      //             if (err) return res.send(500, err + game.name);
      //           });
      //         });
      //       }
      //     });
      // }

      // // User refs
      // //Push queue to orders
      //
      if (doc.user_id != undefined) {
        User.findOne({
          id: parseInt(doc.user_id)
        }, function(err, user) {
          if (err) return res.send(500, err);
          if (!user) {
            console.log("No user found." + doc.user_id);
            //return res.send(404, "No user found." + doc.user_id);
            //return res.send(404, 'El usuario no existe.' +doc.user);
          } else {
            doc.user = mongoose.Types.ObjectId(user._id);
            doc.save(function(err, user) {
              if (err) return res.send(500, "Error saving user: " + err + user._id);
              else {


                if (user['queue'] == undefined) {
                  user.queue = [];
                }

                user.queue.push(doc._id);

                if (user.createdAt === "NULL") {
                  user.createdAt = null;
                }
                user.save(function(err, user) {
                  if (err) return res.send(500, "Error saving user: " + err + user.id);
                  //return res.send(200, user);

                });
              }
            });




          }
        });
      }

    })
    .on('error', function(err) {
      // handle error
    })
    .on('end', function() {
      return res.send(200, 'Queue imported!');
    });

  // User.find({}).stream()
  //   .on('data', function(doc) {
  //     //console.log(doc);
  //     //doc.user = mongoose.Types.ObjectId(user._id);
  //     if(!doc.queue){
  //         doc.queue = [];
  //         doc.save(function(err) {
  //           if (err) console.log(err.message +user.id);
  //
  //         });
  //     }
  //
  //
  //
  //     // handle doc
  //   })
  //   .on('error', function(err) {
  //     // handle error
  //   })
  //   .on('end', function() {
  //     // final callback
  //   });
  // QueueGame.find({}).stream()
  //   .on('data', function(doc) {
  //     //console.log(doc);
  //
  //
  //
  //      if (!isNaN(doc.user)) {
  //       // Game.findOne({
  //       //     id: doc.game
  //       //   })
  //       //   .exec(function(err, game) {
  //       //     if (err) return res.send(500, err);
  //       //     if (!game) {
  //       //       return res.send(404, 'El huego no existe.' +doc.game);
  //       //     } else {
  //       //       console.log(game._id);
  //       //       doc.game = mongoose.Types.ObjectId(game._id);
  //       //       console.log(doc);
  //       //       doc.save(function(err) {
  //       //         if (err) return res.send(500, err.message +doc.id);
  //       //
  //       //       });
  //       //       //return res.status(200).jsonp(user.orders);
  //       //     }
  //       //   });
  //

  //         }
  //     //}
  //
  //     // handle doc
  //   })
  //   .on('error', function(err) {
  //     // handle error
  //   })
  //   .on('end', function() {
  //     // final callback
  //   });

}


function migrateOrders(req, res) {

  Order.find({

  }).limit(2000).stream()
    .on('data', function(doc) {
      console.log(doc.user_id);


      Order.findOne({
        "_id": doc._id
      }, function(err, order) {
        if (doc.user_id !== undefined) {
          User.findOne({
            id: parseInt(order.user_id)
          }, function(err, user) {
            if (err) return res.send(500, err);
            if (!user) {
              //return res.status(404).send("No user found." + order);
              //return res.send(404, 'El usuario no existe.' +doc.user);
              console.log("No user found." + order.user_id);
            } else {
              //console.log(user._id);
              order.user = mongoose.Types.ObjectId(user._id);
              if (order.platform == null) {
                order.platform = 1;
              }

              if (order.phone == null) {
                order.phone = 123456789;
              }
              order.save(function(err, order) {
                if (err) {
                  //res.setHeader('Content-Type', 'application/json');
                  res.status(500).send("Error saving order: " + err.message + doc.id);
                }
                console.log("Order " + doc.id + " updated.");
                //return res.send(200, order);
                // else {
                //
                //
                //   if (user['orders'] == undefined) {
                //     user.orders = [];
                //   }
                //
                //   user.orders.push(doc._id);
                //
                //
                //   if (user.createdAt === "NULL") {
                //     user.createdAt = null;
                //   }
                //   user.save(function(err, user) {
                //     if (err) return res.send(500, "Error saving user: " + err + user.id);
                //     //return res.send(200, user);
                //
                //   });
                // }
              });




            }
          });
        }
      });
    })
    .on('error', function(err) {
      res.status(400).send(err);
    })
    .on('end', function() {
      return res.status(200).send('Orders imported!');
    });


}


function deleteQueueSubscriptions(req, res) {

  User.find({}).stream()
    .on('data', function(doc) {
      //console.log(doc);
      //doc.user = mongoose.Types.ObjectId(user._id);

      //Check if has active subscription
      // If not delete all queue games
      if (doc.queue.length != 0) {
        if (doc.subscriptions !== 'undefined') {
          //Delete queue games
        } else if (doc.subscriptions.length == 0) {
          //  delete queue games
        }
      }



      // handle doc
    })
    .on('error', function(err) {
      // handle error
    })
    .on('end', function() {
      // final callback
    });

}


function migrateDatesQueue(req, res) {

  QueueGame.find({

    })
    .stream()
    .on('data', function(qgame) {
      if (qgame.date_added != 'undefined') {
        qgame.createdAt = qgame.date_added;


        qgame.save(function(err, qgame) {
          if (err) return res.send(500, "Error saving qgame: " + err + qgame.id);
          //res.send(200, qgame);
        });

      } else {
        //res.send(200, qgame);
      }

    })
    .on('error', function(err) {
      // handle error
    })
    .on('end', function() {
      // final callback
      return res.send(200, 'Adresses imported!');
    });



}

function migrateBooleanQueue(req, res) {

  QueueGame.find({

    })
    .stream()
    .on('data', function(qgame) {

      QueueGame.findOne({
        "_id": qgame.id
      }, function(err, queue) {
        if (err) return res.send(500, "Error saving qgame: " + err);
        if (!queue) return res.send(404, "queue not found: " + queue);
        console.log(queue.id)

        var deleted = false;
        var requested = false;

        if (queue.requested == 1) {
          queue.requested = true;
        } else if (queue.requested == 0) {
          queue.requested = false;
        } else {
          return res.send(500, "Error saving qgame: " + queue);
        }

        if (queue.deleted == 0) {

          console.log("NOT deleted");
        } else if (queue.deleted == 1) {
          console.log(" deleted");

          deleted = true;
        } else {
          return res.send(500, "Error saving qgame: " + queue);
        }

        queue.deleted = undefined;
        queue.deleted = deleted;
        queue.requested = undefined;
        queue.requested = requested;

        queue.save(function(err, qgame) {
          if (err) return res.send(500, "Error saving qgame: " + err + qgame.id);
          //res.send(200, qgame);
        });

      });
    })


    //  qgame.deleted = mongoose.Types.Boolean(qgame.deleted.toString());



    //console.log(qgame);
    // qgame.save(function(err, qgame) {
    //   if (err) return res.send(500, "Error saving qgame: " + err + qgame.id);
    //   //res.send(200, qgame);
    // });


    .on('error', function(err) {
      // handle error
      if (err) return res.send(500, "Error saving qgame: " + err);

    })
    .on('end', function() {
      // final callback
      return res.send(200, 'Adresses imported!');
    });



}

function migrateAdresses(req, res) {

  User.find({

    }).stream()
    .on('data', function(doc) {
      var address = {
        "postal1": doc.postal1,
        "postal2": doc.postal2,
        "province": doc.province,
        "zipcode": doc.zipcode,
        "city": doc.city
      };
      User.findOne({
        "_id": doc.id
      }, function(err, user) {
        if (err) return res.send(500, "Error  user: " + err);
        if (!user) return res.send(404, "User not found" + doc.id);
        //console.log(address);

        user.address = address;
        user.postal1 = undefined;
        user.postal2 = undefined;
        user.province = undefined;
        user.zipcode = undefined;
        user.city = undefined;
        user.save(function(err, user) {
          if (err) return res.send(500, "Error saving user: " + err + doc.id);
          console.log('Saved '+doc.id);
          //return res.send(200, user);
        });
      });

    })
    .on('error', function(err) {
      // handle error
    })
    .on('end', function() {
      // final callback
      return res.send(200, 'Adresses imported!');
    });

}

// function migrateOrdersAddress(req, res) {
//
//   Order.find({
//       "status": {
//         $ne: 0
//       }
//
//     }).cursor()
//     .on('data', function(doc) {
//
//
//       var address = {
//         "postal1": doc.postal1,
//         "postal2": doc.postal2,
//         "province": doc.province,
//         "zipcode": doc.zipcode,
//         "city": doc.city
//       };
//
//       Order.findOne({
//         "_id": doc._id
//       }, function(err, order) {
//
//         if (err) return res.send(500, "Error  order: " + err);
//         if (!order) return res.send(404, "order not found" + doc.id);
//
//         var gameIn = [];
//
//         gameIn.push(order.gi1);
//
//         if (order.gi2 > 0) {
//           gameIn.push(order.gi2);
//         }
//
//         if (order.gi3 > 0) {
//           gameIn.push(order.gi3);
//         }
//
//         order.address = address;
//         order.postal1 = undefined;
//         order.postal2 = undefined;
//         order.province = undefined;
//         order.zipcode = undefined;
//         order.city = undefined;
//
//         order.gameIn = gameIn;
//
//         order.save(function(err, order) {
//           if (err) res.send(500, "Error saving order: " + err + doc.id);
//           //return res.send(200, user);
//         });
//       });
//
//     })
//     .on('error', function(err) {
//       // handle error
//     })
//     .on('end', function() {
//       // final callback
//       //return res.send(200, 'Adresses imported!');
//     });
//
// }


async function getGameById(id) {
  console.log("gameeeeee: " + id);
  await Game.findOne({
    id: id
  }).then((result) => {
    return result._id;
  })
  // .then(function(game){
  //   console.log("gm||e: "+game.name);
  // });




}



function migrateOrdersGame1(req, res) {

  Order.find({


    }).cursor()
    .on('data', function(doc) {
      console.log(doc.id);



      Order.findOne({
        "_id": doc._id
      }, function(err, order) {

        if (err) return res.send(500, "Error  order: " + err);
        if (!order) return res.send(404, "order not found" + doc.id);

        var gameIn = [];
        var gameOut = [];
        if (doc.gi1 > 0) {
          Game.findOne({
            id: doc.gi1
          }).then((result, err) => {
            if (!result) return res.send(404, "Game not found: " + doc.gi1);
            if (err) res.send(500, "Error saving order: " + err);
            console.log(result.name);




            order.gamei1 = result._id;
            order.save(function(err, order) {
              if (err) res.send(500, "Error saving order: " + err);
              //return res.send(200, user);
            });
          })
        }

        if (doc.gi2 > 0) {
          Game.findOne({
            id: doc.gi2
          }).then((result) => {
            if (!result) return res.send(404, "Game not found: " + doc.gi2);

            order.gamei2 = result._id;
            order.save(function(err, order) {
              if (err) res.send(500, "Error saving order: " + err);
              //return res.send(200, user);
            });
          })
        }

        if (doc.gi3 > 0) {
          Game.findOne({
            id: doc.gi3
          }).then((result) => {
            if (!result) return res.send(404, "Game not found: " + doc.gi3);

            order.gamei3 = result._id;
            order.save(function(err, order) {
              if (err) res.send(500, "Error saving order: " + err);
              //return res.send(200, user);
            });
          })
        }

        if (doc.go1 > 0) {
          Game.findOne({
            id: doc.go1
          }).then((result) => {
            if (!result) return res.send(404, "Game not found: " + doc.go1);

            order.gameo1 = result._id;
            order.save(function(err, order) {
              if (err) res.send(500, "Error saving order: " + err);
              //return res.send(200, user);
            });
          });
        }
        if (doc.go2 > 0) {
          Game.findOne({
            id: doc.go2
          }).then((result) => {
            if (!result) return res.send(404, "Game not found: " + doc.go2);

            order.gameo2 = result._id;
            order.save(function(err, order) {
              if (err) res.send(500, "Error saving order: " + err);
              //return res.send(200, user);
            });
          });
        }

        if (doc.go3 > 0) {
          Game.findOne({
            id: doc.go3
          }).then((result) => {
            if (!result) return res.send(404, "Game not found: " + doc.go3);

            order.gameo3 = result._id;
            order.save(function(err, order) {
              if (err) res.send(500, "Error saving order: " + err);
              //return res.send(200, user);
            });
          });
        }

      });


    })
    .on('error', function(err) {
      // handle error
      return res.send(400, err);
    })
    .on('end', function() {
      // final callback
      //return res.send(200, 'Games1 imported!');
    });

}

function migrateOrdersGame2(req, res) {

  Order.find({


    }).cursor()
    .on('data', function(doc) {

      Order.findOne({
        "_id": doc._id
      }, function(err, order) {

        if (err) return res.send(500, "Error  order: " + err);
        if (!order) return res.send(404, "order not found" + doc.id);

        var gameIn = [];
        var gameOut = [];

        if (order.gamei1 != undefined) {
          gameIn.push(order.gamei1);
        }

        if (order.gamei2 != undefined) {
          gameIn.push(order.gamei2);
        }

        if (order.gamei3 != undefined) {
          gameIn.push(order.gamei3);
        }

        if (order.gameo1 != undefined) {
          gameOut.push(order.gameo1);
        }

        if (order.gameo2 != undefined) {
          gameOut.push(order.gameo2);
        }

        if (order.gameo3 != undefined) {
          gameOut.push(order.gameo3);
        }

        order.gameIn = gameIn;
        order.gameOut = gameOut;

        order.save(function(err, order) {
          if (err) res.send(500, "Error saving order: " + err + doc.id);
          //     //return res.send(200, user);
        });
      });



    })
    .on('error', function(err) {
      // handle error
    })
    .on('end', function() {
      // final callback
      //return res.send(200, 'Adresses imported!');
    });

}


function migrateAdressesOrder(req, res) {

  Order.find({


    }).cursor()
    .on('data', function(doc) {

      var address = {
        "postal1": doc.postal1,
        "postal2": doc.postal2,
        "province": doc.province,
        "zipcode": doc.zipcode,
        "city": doc.city
      };
      Order.findOne({
        "_id": doc._id
      }, function(err, order) {

        if (err) return res.send(500, "Error  order: " + err);
        if (!order) return res.send(404, "order not found" + doc.id);

        //  console.log(address);
        order.createdAt = Date(order.date);
        order.address = address;
        // order.postal1 = undefined;
        // order.postal2 = undefined;
        // order.province = undefined;
        // order.zipcode = undefined;
        // order.city = undefined;
        console.log(order);
        order.save(function(err, order) {
          if (err) res.send(500, "Error saving order: " + err + doc.id);
          //return res.send(200, user);
        });
      });

    })
    .on('error', function(err) {
      // handle error
    })
    .on('end', function() {
      // final callback
      //return res.send(200, 'Adresses imported!');
    });

}


function invalidateQueue(req, res) {
  QueueGame.find({
      $and: [{
          requested: false
        },
        {
          deleted: false
        }
      ]

    })
    .populate({
      path: 'user',
      populate: {
        path: 'subscriptions'
      }
    })
    .stream()
    .on('data', function(qgame) {
      //   console.log(qgame.user.name);
      //
      //   //res.send(doc);
      //   var valid = false;
      //
      //res.send(200, qgame);
      if (qgame.user.subscriptions.length > 0) {

        var status = qgame.user.subscriptions[qgame.user.subscriptions.length - 1].status;
        if (status == "canceled") {

          qgame.deleted = true;
          qgame.save(function(err) {
            if (err) res.send(500, err);
            //res.send("no sub" + qgame);

          });
        } else {
          console.log("NICE");
        }
      } else {
        qgame.deleted = true;
        qgame.save(function(err) {
          if (err) res.send(500, err);
          //res.send("no sub" + qgame);

        });
      }

    })
    .on('error', function(err) {
      res.send(500, err);
    })
    .on('end', function() {
      // final callback
      res.send(200, "i ja estaria");

    });
}



function migrateSubscriptions(req, res) {

  //Subscriptions
  Subscription.find().cursor()
    .on('data', function(doc) {

      var user_id = doc.user_id;
      var subs = doc.subscriptionId;

      Subscription.findOne({
        "_id": doc._id
      }, function(err, sub) {

        // console.log(user_id);
        console.log('userID ' + user_id);

        User.findOne({
          "id": parseInt(user_id)
        }, function(err, user) {
          if (err) return res.send(500, 'owo' + err + user_id);
          if (!user) {
            //return res.send(404, "User not found" + user_id);
            console.log("User not found" + user_id);
            //return res.send(404, 'El usuario no existe.' +doc.user);
          } else {
            console.log(sub);

            sub.user = user._id;
            // doc.subscriptionId = subs;
            console.log('subID ' + sub._id);

            console.log('userID ' + user._id);
            // sub.subscriptionId = sub._id;
            if (sub.status == undefined) {
              sub.status = 'canceled';
            }
            //return res.send(200, sub);
            sub.save(function(err, subs) {
              if (err) return res.send(500, 'uwu' + err + user_id);

              if (user['subscriptions'] == undefined) {
                user.subscriptions = [];
              }

              user.subscriptions.push(doc._id);
              user.save(function(err) {
                if (err) return res.send(500, 'owO' + err);

              });

            });

          }


        });
      });

    })
    .on('error', function(err) {
      return res.send(400, err);
    })
    .on('end', function() {
      //  return res.send(200, 'Subscriptions imported!');
      // final callback
    });
}

function migrateSubscriptions2(req, res) {

  //Subscriptions
  Subscription.find({}).stream()
    .on('data', function(doc) {

      var user_id = doc.user_id;
      var subs = doc.subscriptionId;

      User.findOne({
        "id": parseInt(user_id)
      }, function(err, user) {
        if (err) return res.send(500, err + user_id);
        if (!user) {
          return res.send(404, "User not found" + user_id);
          //return res.send(404, 'El usuario no existe.' +doc.user);
        } else {

          doc.user = user._id;
          doc.subscriptionId = doc.subscription_id;
          // console.log(subs);
          doc.save(function(err) {
            if (err) return res.send(500, err + user_id);

            if (user['subscriptions'] == undefined) {
              user.subscriptions = [];
            }

            user.subscriptions.push(doc._id);
            user.save(function(err) {
              if (err) return res.send(500, err);

            });

          });

        }

      });

    })
    .on('error', function(err) {
      return res.send(400, err);
    })
    .on('end', function() {
      return res.send(200, 'Subscriptions imported!');
      // final callback
    });
}


function migrateTokens(req, res) {

  Token.find({
      user_id: {
        $exists: true
      }
    }).stream()
    .on('data', function(doc) {

      User.findOne({
        "id": parseInt(doc.user_id)
      }, function(err, user) {
        if (err) return res.status(500).send(err + doc.user_id);
        if (!user) {
          return res.status(404).send("User not found" + doc.user_id);
          //return res.send(404, 'El usuario no existe.' +doc.user);
        } else {

          doc.user = user._id;
          doc.save(function(err) {
            if (err) return res.send(500, err + doc.user_id);

          });

        }
      });

    })
    .on('error', function(err) {
      return res.send(400, err);
    })
    .on('end', function() {
      return res.send(200, 'Subscriptions imported!');
      // final callback
    });

}


function migrateUserGame1(req, res) {

  User.find({


    }).cursor()
    .on('data', function(doc) {
      console.log(doc.id);



      User.findOne({
        "_id": doc._id
      }, function(err, user) {

        if (err) return res.send(500, "Error  order: " + err);
        if (!user) return res.send(404, "user not found" + doc.id);

        var gameIn = [];
        var gameOut = [];
        if (doc.idGame1 > 0) {
          Game.findOne({
            id: doc.idGame1
          }).then((result, err) => {
            if (!result) return res.send(404, "Game not found: " + doc.idGame1);
            if (err) res.send(500, "Error saving order: " + err);
            console.log(result.name);




            user.gamei1 = result._id;
            user.save(function(err, user) {
              if (err) res.send(500, "Error saving user: " + err);
              //return res.send(200, user);
            });
          })
        }

        if (doc.idGame2 > 0) {
          Game.findOne({
            id: doc.idGame2
          }).then((result) => {
            if (!result) return res.send(404, "Game not found: " + doc.idGame2);

            user.gamei2 = result._id;
            user.save(function(err, user) {
              if (err) res.send(500, "Error saving user: " + err);
              //return res.send(200, user);
            });
          })
        }

        if (doc.idGame3 > 0) {
          Game.findOne({
            id: doc.idGame3
          }).then((result) => {
            if (!result) return res.send(404, "Game not found: " + doc.idGame3);

            user.gamei3 = result._id;
            user.save(function(err, order) {
              if (err) res.send(500, "Error saving user: " + err);
              //return res.send(200, user);
            });
          })
        }
      });


    })
    .on('error', function(err) {
      // handle error
      return res.send(400, err);
    })
    .on('end', function() {
      // final callback
      //return res.send(200, 'Games1 imported!');
    });

}

function migrateUserGame2(req, res) {

  User.find({


    }).cursor()
    .on('data', function(doc) {

      User.findOne({
        "_id": doc._id
      }, function(err, user) {

        if (err) return res.send(500, "Error  order: " + err);
        if (!user) return res.send(404, "user not found" + doc.id);

        var gameIn = [];


        if (user.gamei1 != undefined) {
          gameIn.push(user.gamei1);
        }

        if (user.gamei2 != undefined) {
          gameIn.push(user.gamei2);
        }

        if (user.gamei3 != undefined) {
          gameIn.push(user.gamei3);
        }


        user.games = gameIn;


        user.save(function(err, user) {
          if (err) res.send(500, "Error saving order: " + err + doc.id);

          console.log(user._id);

          //     //return res.send(200, user);
        });
      });



    })
    .on('error', function(err) {
      // handle error
    })
    .on('end', function() {
      // final callback
      //return res.send(200, 'Adresses imported!');
    });


}




module.exports = {
  migrateQueue,
  migrateOrders,
  migrateOrdersGame1,
  migrateSubscriptions,
  migrateSubscriptions2,
  migrateAdresses,
  migrateGames,
  migrateUserGame1,
  migrateUserGame2,
  invalidateQueue,
  migrateDatesQueue,
  migrateBooleanQueue,
  migrateOrdersGame2,
  migrateAdressesOrder,
  dropGamesQueue,
  migrateTokens
}
