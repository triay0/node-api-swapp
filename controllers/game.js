var Game = require('../models/game');
var Genre = require('../models/genre');
var User = require('../models/user');
var Review = require('../models/review');
var QueueGame = require('../models/queueGame');
var NotificationsCtrl = require('../controllers/notifications');
var Order = require('../models/order');
var mongoose = require('mongoose');

const discount = 0.7;

var moment = require('moment');
var async = require('async');
const Nightmare = require('nightmare');

var sk_stripe_test = 'sk_test_E1SdK829cuZtucs5qI5dwQoU';
var sk_stripe_live = 'sk_live_guvFbTPzk0ZWZry6Io7JAddv';

var stripe = require("stripe")(sk_stripe_live);


/*** Metodo para obtener todos los juegos ***/
function findAllGames(req, res) {
  Game.aggregate([{
      $lookup: {
        from: "queuegames",
        localField: "queue",
        foreignField: "_id",
        as: "queue"
      }
    },
    {
      $lookup: {
        from: "genres",
        localField: "genres",
        foreignField: "_id",
        as: "genres"
      }
    },
    {
      $match: {
        "relased": 1
      }
    },
    {
      $sort: {
        "name": 1
      }
    },
    {
      "$project": {
        "name": 1,
        "id": 1,
        "cover": 1,
        "screenshot": 1,
        "summary": 1,
        "pegi": {
          '$toInt': '$pegi'
        },
        "summary": 1,
        "genres": 1,
        "stock": 1,
        "price_game": 1,
        "booking": 1,
        "releaseDate": 1,
        "sku_game": 1,
        "sku_cex": 1,
        "queueDetail": {
          "$filter": {
            "input": "$queue",
            "as": "que",
            "cond": {
              $and: [{
                  $eq: ["$$que.deleted", false]
                },
                {
                  $eq: ["$$que.requested", false]
                }
              ]
            }
          },
        },
        "queueNum": {
          "$size": {
            "$filter": {
              "input": "$queue",
              "as": "que",
              "cond": {
                $and: [{
                    $eq: ["$$que.deleted", false]
                  },
                  {
                    $eq: ["$$que.requested", false]
                  }
                ]
              }
            }
          }
        },
        "queue": {
          "$subtract": ["$stock", {
            "$size": {
              "$filter": {
                "input": "$queue",
                "as": "que",
                "cond": {
                  $and: [{
                      $eq: ["$$que.deleted", false]
                    },
                    {
                      $eq: ["$$que.requested", false]
                    }
                  ]
                }
              }
            }
          }]
        }
      },
    }
  ], function(err, games) {
    if (err) res.send(500, err.message);
    res.status(200).jsonp(games);
  });
};

/*** Metodo para obtener todos los juegos con stock ***/
function findGamesStock(req, res) {

  Game.aggregate([{
        $lookup: {
          from: "queuegames",
          localField: "queue",
          foreignField: "_id",
          as: "queue"
        }
      },
      {
        $lookup: {
          from: "genres",
          localField: "genres",
          foreignField: "_id",
          as: "genres"
        }
      },
      {
        $match: {
          'stock': {
            $gt: 0
          },
          "relased": 1
        }
      },
      {
        $sort: {
          "name": 1
        }
      },
      {
        "$project": {
          "name": 1,
          "id": 1,
          "cover": 1,
          "screenshot": 1,
          "summary": 1,
          "pegi": {
            '$toInt': '$pegi'
          },
          "summary": 1,
          "genres": 1,
          "sku_game": 1,
          "sku_cex": 1,
          "prevPrice": "$price_game",
          "price_game": {
            $let: {
              vars: {
                'priceD': {
                  $multiply: ["$price_game", discount]
                },
              },
              in: {
                $divide: [{
                  $trunc: {
                    $multiply: ["$$priceD", 100]
                  }
                }, 100]
              }
            },
          },
          "booking": 1,
          "releaseDate": 1,
          "queueNum": {
            "$size": {
              "$filter": {
                "input": "$queue",
                "as": "que",
                "cond": {
                  $and: [{
                      $eq: ["$$que.deleted", false]
                    },
                    {
                      $eq: ["$$que.requested", false]
                    }
                  ]
                }
              }
            }
          },
          "queue": {
            "$subtract": ["$stock", {
              "$size": {
                "$filter": {
                  "input": "$queue",
                  "as": "que",
                  "cond": {
                    $and: [{
                        $eq: ["$$que.deleted", false]
                      },
                      {
                        $eq: ["$$que.requested", false]
                      }
                    ]
                  }
                }
              }
            }]
          }
        },
      }
    ],
    function(err, games) {
      if (err) res.send(500, err.message);
      return res.status(200).send(games);
    });
};

/*** Metodo para buscar juegos a través del nombre ***/
function findByName(req, res) {

  Game.aggregate([{
      "$match": {
        "name": {
          '$regex': req.params.id
        }
      }
    },
    {
      "$lookup": {
        "from": "queuegames",
        "localField": "queue",
        "foreignField": "_id",
        "as": "queue_result",
      }
    },
    {
      "$lookup": {
        "from": "users",
        "localField": "queuegames.user",
        "foreignField": "_id",
        "as": "user_result",
      }
    },
    {
      "$project": {
        "_id": 1,
        "name": 1,
        "cover": 1,
        "screenshot": 1,
        "summary": 1,
        "stock": 1,
        "pegi": 1,
        "user_result": 1,
        "queue_size": {
          $size: {
            $filter: {
              input: "$queue_result",
              as: "item",
              cond: {
                $and: [{
                    $eq: ["$$item.deleted", false]
                  },
                  {
                    $eq: ["$$item.requested", false]
                  }
                ]
              }
            },
          }
        },
        "queue_result": {

          $filter: {
            input: "$queue_result",
            as: "item",
            cond: {
              $and: [{
                  $eq: ["$$item.deleted", false]
                },
                {
                  $eq: ["$$item.requested", false]
                }
              ]
            }
          },

        },
      }
    }
  ], function(err, games) {
    if (err) res.send(500, err.message);
    res.status(200).jsonp(games);
  });
};

/*** Metodo para comprar uno o varios juegos ***/
function buyGame(req, res) {

  var params = req.body;
  var id;

  User.findOne({
      uid: req.user.uid
    })
    .exec(function(err, user) {
      if (err) return res.send(500, err.message);
      if (!user) {
        return res.send(404, 'El usuario no existe.');
      } else {
        var order = new Order();
        order.user = user.id;
        order.name = params.name;
        order.phone = params.phone;
        order.gameIn = params.gameIn;
        order.address.postal1 = params.postal1;
        order.address.number = params.number;
        order.address.postal2 = params.postal2;
        order.address.province = params.province;
        order.address.zipcode = params.zipcode;
        order.address.city = params.city;
        order.comment = params.comment;
        order.createdAt = moment().format();
        order.statusFlutter = 0;
        order.status = 1;
        order.buy = true;
        order.platform = params.platform;
        order.timeZone = params.timeZone;

        //  var price =  getPriceGames(order.gameIn);
        var price = 0;
        var nameGames = '';
        console.log(order.gameIn);
        async.forEach(order.gameIn, function(game, callback) {

          if (game == null) {
            price = price + 3.99;
            //console.log('Price: ' + price);
            callback();
            return;
          }

          Game.findOne({
            _id: game
          }, function(err, gameAux) {
            if (err) return res.send(500, err);
            if (!gameAux) return res.send(404, 'Game not found.');
            if (!gameAux.price_game) return res.send(404, 'Game price not found.');
            nameGames = gameAux.name + nameGames + '';
            price = price + (gameAux.price_game * discount);

            callback();

          });
        }, function(err) {
          console.log(price + '€');
          if (params.token !== undefined) {

            stripe.customers.createSource(user.stripe_id, {
              source: params.token
            }, function(err, card) {
              if (err) return res.send(500, err.message);
              stripe.customers.update(user.stripe_id, {
                default_source: card.id
              });
              //res.status(200).jsonp(card);
              createCharge(price, nameGames, order, user, req, res);
            });
          } else {
            createCharge(price, nameGames, order, user, req, res);
          }
        });
      }
    });
}

/*** Metodo para hacer un cargo en la tarjeta ***/
function createCharge(price, nameGames, order, user, req, res) {

  if (price == 0) {
    return res.send(500, 'Error interno contacta con Swapp.');
  }
  price = Number(price.toFixed(2)) * 100;
  price = Math.floor(price)

  stripe.invoiceItems.create({
    amount: price,
    currency: "eur",
    description: nameGames,
    customer: user.stripe_id,

  }, function(err, invoiceItem) {
    if (err) {
      return res.send(500, err);
    }
    stripe.invoices.create({
      customer: user.stripe_id,
      description: nameGames,
    }, function(err, invoice) {
      if (err) {
        return res.send(500, err);
      }
      stripe.invoices.pay(invoice.id, function(err, invoice) {
        if (err) {
          return res.send(500, err);
        }
        order.save(function(err, order) {
          if (err) return res.send(500, err);
          if (!order) return res.status(404).send({
            message: 'El pedido no ha sido guardado'
          });
          else {
            order.save(function(err, order) {
              if (err) return res.send(500, err);
              if (!order) return res.status(404).send({
                message: 'El pedido no ha sido guardado'
              });
              else {
                user.orders.push(order._id);
                user.save(function(err) {
                  if (err) return res.send(500, err);

                  async.forEach(order.gameIn, function(game, callback) {

                    Game.findOne({
                      _id: game
                    }, function(err, gameAux) {
                      if (err) return res.send(500, err);
                      if (!gameAux) return res.send(404, 'Game not found.');
                      if (gameAux.stock > 0) {
                        console.log('get Dawn get Dawn');
                        gameAux.stock = gameAux.stock - 1;
                        gameAux.save(function(err, game) {
                          if (err) return res.send(500, err.message);
                          callback();
                        });
                      } else {
                        callback();
                      }
                    });
                  }, function(err) {
                    if (err) return res.send(500, err);

                    res.status(200).jsonp(order);
                    var message = 'SALE: ' + nameGames + ' ' + price + ' €';
                    return NotificationsCtrl.newAlert(message);
                  });
                });
              }
            });
          }
        });
      });
    });
  });
}

/*** Metodo para obtener las novedades ***/
function findPremieres(req, res) {
  Game.find({
    premium: {
      eq: true
    }
  }, function(err, games) {
    if (err) res.status(500).body(err.message);
    console.log('GET /games/premieres/');
    res.status(200).jsonp(games);
  });
};

/*** Metodo para obtener obtener la vista principal ***/
function findMain(req, res) {

  var main = [];
  var lists = [];


  async.parallel({
      genres: function(genres) {
        Genre.find({
            icon: {
              $ne: null
            }
          })
          .exec(genres);
      },
      modelAFind: function(cb) {
        Game.aggregate([{
            $lookup: {
              from: "queuegames",
              localField: "queue",
              foreignField: "_id",
              as: "queue"
            }
          },
          {
            $lookup: {
              from: "genres",
              localField: "genres",
              foreignField: "_id",
              as: "genres"
            }
          },

          {
            $sort: {
              id: -1
            }
          },
          {
            $match: {
              premiere: {
                $ne: true
              }
            }
          },
          {
            $limit: 7
          },
          {
            "$project": {
              "name": 1,
              "cover": 1,
              "screenshot": 1,
              "summary": 1,
              "pegi": {
                '$toInt': '$pegi'
              },
              "summary": 1,
              "genres": 1,
              "booking": 1,
              "releaseDate": 1,
              "price_game": 1,

              "stock": 1,
              "queueNum": {
                "$size": {
                  "$filter": {
                    "input": "$queue",
                    "as": "que",
                    "cond": {
                      $and: [{
                          $eq: ["$$que.deleted", false]
                        },
                        {
                          $eq: ["$$que.requested", false]
                        }
                      ]
                    }
                  }
                }
              },
              "queue": {
                "$subtract": ["$stock", {
                  "$size": {
                    "$filter": {
                      "input": "$queue",
                      "as": "que",
                      "cond": {
                        $and: [{
                            $eq: ["$$que.deleted", false]
                          },
                          {
                            $eq: ["$$que.requested", false]
                          }
                        ]
                      }
                    }
                  }
                }]
              }
            },
          }
        ]).exec(cb);
      },
      nextReleases: function(cb2) {
        Game.aggregate([{
            $lookup: {
              from: "queuegames",
              localField: "queue",
              foreignField: "_id",
              as: "queue"
            }
          },
          {
            $lookup: {
              from: "genres",
              localField: "genres",
              foreignField: "_id",
              as: "genres"
            }
          },

          {
            $sort: {
              id: -1
            }
          },
          {
            $match: {
              id: {
                $gt: 516
              }
            }
          },
          {
            "$project": {
              "name": 1,
              "cover": 1,
              "screenshot": 1,
              "summary": 1,
              "pegi": {
                '$toInt': '$pegi'
              },
              "summary": 1,
              "genres": 1,
              "price_game": 1,

              "stock": 1,
              "releaseDate": 1,
              "booking": 1,
              "queueNum": {
                "$size": {
                  "$filter": {
                    "input": "$queue",
                    "as": "que",
                    "cond": {
                      $and: [{
                          $eq: ["$$que.deleted", false]
                        },
                        {
                          $eq: ["$$que.requested", false]
                        }
                      ]
                    }
                  }
                }
              },
              "queue": {
                '$toInt': '-1'
              },

            },
          }
        ]).exec(cb2);
      },

      assassins: function(assa) {
        Game.aggregate([{
            $lookup: {
              from: "queuegames",
              localField: "queue",
              foreignField: "_id",
              as: "queue"
            }
          },

          {
            $match: {
              genres: mongoose.Types.ObjectId("5c73b916da65e31ee8f09fb6")
            }
          },
          {
            $lookup: {
              from: "genres",
              localField: "genres",
              foreignField: "_id",
              as: "genres"
            }
          },
          {
            "$project": {
              "name": 1,
              "cover": 1,
              "screenshot": 1,
              "summary": 1,
              "pegi": {
                '$toInt': '$pegi'
              },
              "summary": 1,
              "genres": 1,
              "stock": 1,
              "booking": 1,
              "price_game": 1,

              "queueNum": {
                "$size": {
                  "$filter": {
                    "input": "$queue",
                    "as": "que",
                    "cond": {
                      $and: [{
                          $eq: ["$$que.deleted", false]
                        },
                        {
                          $eq: ["$$que.requested", false]
                        }
                      ]
                    }
                  }
                }
              },
              "queue": {
                "$subtract": ["$stock", {
                  "$size": {
                    "$filter": {
                      "input": "$queue",
                      "as": "que",
                      "cond": {
                        $and: [{
                            $eq: ["$$que.deleted", false]
                          },
                          {
                            $eq: ["$$que.requested", false]
                          }
                        ]
                      }
                    }
                  }
                }]
              }
            },
          }
        ]).exec(assa);
      },
      mainGame: function(mainGame) {
        Game.aggregate([{
            $lookup: {
              from: "queuegames",
              localField: "queue",
              foreignField: "_id",
              as: "queue"
            }
          },

          {
            $match: {
              _id: mongoose.Types.ObjectId("5c5c63a73393270a2444086c")
            }
          },
          {
            $lookup: {
              from: "genres",
              localField: "genres",
              foreignField: "_id",
              as: "genres"
            }
          },
          {
            "$project": {
              "name": 1,
              "cover": 1,
              "screenshot": 1,
              "summary": 1,
              "pegi": {
                '$toInt': '$pegi'
              },
              "summary": 1,
              "genres": 1,
              "stock": 1,
              "booking": 1,
              "price_game": 1,

              "queueNum": {
                "$size": {
                  "$filter": {
                    "input": "$queue",
                    "as": "que",
                    "cond": {
                      $and: [{
                          $eq: ["$$que.deleted", false]
                        },
                        {
                          $eq: ["$$que.requested", false]
                        }
                      ]
                    }
                  }
                }
              },
              "queue": {
                "$subtract": ["$stock", {
                  "$size": {
                    "$filter": {
                      "input": "$queue",
                      "as": "que",
                      "cond": {
                        $and: [{
                            $eq: ["$$que.deleted", false]
                          },
                          {
                            $eq: ["$$que.requested", false]
                          }
                        ]
                      }
                    }
                  }
                }]
              }
            },
          }
        ]).exec(mainGame);
      },
      cod: function(cod) {
        Game.aggregate([{
            $lookup: {
              from: "queuegames",
              localField: "queue",
              foreignField: "_id",
              as: "queue"
            }
          },

          {
            $match: {
              genres: mongoose.Types.ObjectId("5c73bf8806cb2d335c6bd9bc")
            }
          },
          {
            $lookup: {
              from: "genres",
              localField: "genres",
              foreignField: "_id",
              as: "genres"
            }
          },
          {
            "$project": {
              "name": 1,
              "cover": 1,
              "screenshot": 1,
              "booking": 1,

              "summary": 1,
              "pegi": {
                '$toInt': '$pegi'
              },
              "summary": 1,
              "genres": 1,
              "stock": 1,
              "price_game": 1,

              "queueNum": {
                "$size": {
                  "$filter": {
                    "input": "$queue",
                    "as": "que",
                    "cond": {
                      $and: [{
                          $eq: ["$$que.deleted", false]
                        },
                        {
                          $eq: ["$$que.requested", false]
                        }
                      ]
                    }
                  }
                }
              },
              "queue": {
                "$subtract": ["$stock", {
                  "$size": {
                    "$filter": {
                      "input": "$queue",
                      "as": "que",
                      "cond": {
                        $and: [{
                            $eq: ["$$que.deleted", false]
                          },
                          {
                            $eq: ["$$que.requested", false]
                          }
                        ]
                      }
                    }
                  }
                }]
              }
            },
          }
        ]).exec(cod);
      }
    },
    function(err, result) {


      // main.genres.push(
      //   result.genres
      // );
      var ret = {
        title: "Últimas novedades",
        games: result.modelAFind
      };
      lists.push(
        ret
      );

      var ret2 = {
        title: "Próximos lanzamientos",
        games: result.nextReleases
      };
      lists.push(
        ret2
      );

      var assassins = {
        title: "Assassin's Creed",
        games: result.assassins
      };
      lists.push(
        assassins
      );
      var cod = {
        title: "Call Of Duty",
        games: result.cod
      };
      lists.push(
        cod
      );

      main.push({
        "main": result.mainGame[0],
        "genres": result.genres,
        "lists": lists
      });


      res.status(200).jsonp(main[0]);
    });
};

/*** Metodo para obtener todos los juegos de un género determinado ***/
function findByGenre(req, res) {
  Game.aggregate([{
      $lookup: {
        from: "queuegames",
        localField: "queue",
        foreignField: "_id",
        as: "queue"
      }
    },

    {
      $match: {
        genres: mongoose.Types.ObjectId(req.params.id)
      }
    },
    {
      $lookup: {
        from: "genres",
        localField: "genres",
        foreignField: "_id",
        as: "genres"
      }
    },
    {
      "$project": {
        "name": 1,
        "cover": 1,
        "screenshot": 1,
        "summary": 1,
        "pegi": {
          '$toInt': '$pegi'
        },
        "summary": 1,
        "genres": 1,
        "stock": 1,
        "booking": 1,
        "price_game": 1,

        "queueNum": {
          "$size": {
            "$filter": {
              "input": "$queue",
              "as": "que",
              "cond": {
                $and: [{
                    $eq: ["$$que.deleted", false]
                  },
                  {
                    $eq: ["$$que.requested", false]
                  }
                ]
              }
            }
          }
        },
        "queue": {
          "$subtract": ["$stock", {
            "$size": {
              "$filter": {
                "input": "$queue",
                "as": "que",
                "cond": {
                  $and: [{
                      $eq: ["$$que.deleted", false]
                    },
                    {
                      $eq: ["$$que.requested", false]
                    }
                  ]
                }
              }
            }
          }]
        }
      },
    }
  ]).exec(function(err, games) {
    if (err) return res.send(500, err.message);
    if (!games) {
      return res.send(404, 'Not games found');
    } else {
      return res.status(200).jsonp(games);
    }
  });
};

/*** Metodo para obtener un juego con un id determinado ***/
function findById(req, res) {

  var id = mongoose.Types.ObjectId(req.params.id);

  Game.aggregate([{
        $lookup: {
          from: "queuegames",
          localField: "queue",
          foreignField: "_id",
          as: "queue_result"
        }
      },
      {
        $lookup: {
          from: "genres",
          localField: "genres",
          foreignField: "_id",
          as: "genres"
        }
      },
      // {
      //   $unwind: "$queue"
      // },
      {
        $match: {
          "_id": id,
        }
      },
      {
        "$project": {
          "_id": 1,
          "name": 1,
          "id": 1,
          "cover": 1,
          "screenshot": 1,
          "summary": 1,
          "stock": 1,
          "genres": 1,
          "booking": 1,
          "releaseDate": 1,
          "pegi": {
            '$toInt': '$pegi'
          },
          "user_result": 1,
          "queue": {
            "$subtract": ["$stock", {
              "$size": {
                "$filter": {
                  "input": "$queue_result",
                  "as": "que",
                  "cond": {
                    $and: [{
                        $eq: ["$$que.deleted", false]
                      },
                      {
                        $eq: ["$$que.requested", false]
                      }
                    ]
                  }
                }
              }
            }]
          },
          "queue_size": {
            //$size: {
            $filter: {
              input: "$queue_result",
              as: "item",
              cond: {
                $and: [{
                    $eq: ["$$item.deleted", false]
                  },
                  {
                    $eq: ["$$item.requested", false]
                  }
                ]
              }
            },
          }
        }
      }
    ]

    ,
    function(err, game) {
      if (err) return res.send(500, "Error al realizar la petición." + err.message);
      if (!game) return res.send(404, 'No existe el juego.');
      //  game.queueSize = game.queue.length;

      console.log('GET /games/' + req.params.id);
      res.status(200).jsonp(game[0]);
    });

};

function moveQueue(req, res) {
  console.log('jej');
  var games = findAllGames();
  res.status(200).jsonp(games);

}

/*** Metodo para guardar un nuevo juego ***/
function saveGame(req, res) {
  var game = new Game({
    name: req.body.name,
    id: req.body.id,
    released: req.body.released,
    cover: req.body.cover,
    screenshot: req.body.screenshot,
    genre: req.body.genre,
    summary: req.body.summary,
    main: req.body.main,
    mainCateg: req.body.mainCateg,
    pegi: req.body.pegi,
    premium: req.body.premium
  });

  game.save(function(err, game) {
    if (err) return res.send(500, err.message);
    res.status(200).jsonp(game);
  });
};



/*** Metodo para actualizar un juego ***/
function updateGame(req, res) {
  Game.findById(req.params.id, function(err, game) {
    if (!game) return res.send(404, 'Game not found.');

    game.name = req.body.name;
    game.id = req.body.id;
    game.released = req.body.released;
    game.cover = req.body.cover;
    game.screenshot = req.body.screenshot;
    game.summary = req.body.summary;
    game.main = req.body.main;
    game.mainCateg = req.body.mainCateg;
    game.pegi = req.body.pegi;
    game.stock = req.body.stock;
    game.premium = req.body.premium;

    if (req.body.genre2 !== null && req.body.genre2 !== "") {
      game.genres.push(req.body.genre2);
    }


    game.save(function(err) {
      if (err) return res.send(500, err.message);
      res.status(200).jsonp(game);
    });
  });
};

/*** Metodo para borrar un juego ***/
function deleteGame(req, res) {
  Game.findById(req.params.id, function(err, game) {
    game.remove(function(err) {
      if (err) return res.send(500, err.message);
      res.status(200);
    })
  });
};

/*** Metodo para añadir un juego en la cola ***/
function addGameQueue(req, res) {
  var params = req.body;
  var inQueue = false;

  QueueGame.find({
      user: req.user._id,
      deleted: {
        $eq: false
      },
      requested: {
        $eq: false
      }
    }, 'createdAt')
    .populate('game', 'name cover')
    .exec(function(err, queue) {
      if (err) res.status(400).send(err);
      if (queue) {
        if (queue.length > 2) {
          res.status(400).send("Superado numero de juegos en cola.");
        } else {
          for (var i = 0; i < queue.length; i++) {
            console.log(queue[i].game._id);
            if (queue[i].game._id == req.params.id) {
              inQueue = true;
              return res.status(400).send('El juego ya está en la cola.');
            }
          }

          if (!inQueue) {
            var queueGame = new QueueGame({
              game: req.params.id,
              user: req.user._id,
              requested: false,
              deleted: false,
              createdAt: moment(),
              platform: params.platform
            })

            queueGame.save(function(err, qGame) {
              if (err) return res.send(500, err);
              if (!qGame) return res.send(404, 'No qGame');
              Game.findById(req.params.id,
                function(err, game) {

                  if (err) return res.send(500, err);
                  if (!game) return res.send(404, 'No game');
                  game.queue.push(qGame._id);
                  game.save(function(err) {
                    if (err) return res.send(500, err + game.name);
                    return res.status(200).jsonp(game);
                  });
                });
            });
          }
        }
      } else {
        var queueGame = new QueueGame({
          game: req.params.id,
          user: req.user._id,
          requested: false,
          deleted: false,
          createdAt: moment(),
          platform: params.platform
        })

        queueGame.save(function(err, qGame) {
          if (err) return res.send(500, err);
          if (!qGame) return res.send(404, 'No qGame');
          Game.findById(req.params.id,
            function(err, game) {
              if (err) return res.send(500, err);
              if (!game) return res.send(404, 'No game');
              console.log(game);
              game.queue.push(qGame._id);
              game.save(function(err) {
                if (err) return res.send(500, err + game.name);
                return res.status(200).jsonp(game);
              });
            });
        });
      }
    });
}

/*** Metodo para añadir un juego en la cola desde el backend anterior ***/
function addGameQueueMYSQL(req, res) {

  let params = req.body;

  async.parallel({
      game: function(cb) {
        Game.findOne({
            id: params.game_id
          })
          .exec(cb);
      },
      user: function(cb2) {
        User.findOne({
          id: parseInt(params.user_id)
        }).exec(cb2);
      }
    },
    function(err, result) {
      if (result.user == null) {
        var message = '404 : User not exists ' + params.user_id;
        return NotificationsCtrl.newAlert(message);
      }
      if (result.game == null) {
        var message = '404 : Game not exists ' + params.game_id;
        return NotificationsCtrl.newAlert(message, res);
      }

      var queueGame = new QueueGame({
        game: result.game._id,
        user: result.user._id,
        requested: false,
        deleted: false,
        createdAt: moment(),
        platform: params.platform
      })
      queueGame.save(function(err, qGame) {
        if (err) return res.send(500, err);
        Game.findOne({
            id: params.game_id
          }),
          function(err, game) {
            game.queue.push(doc._id);
            game.save(function(err) {
              if (err) return res.send(500, err + game.name);
              return res.status(200).jsonp(qGame);
            });
          };

      });
    });
};

/*** Metodo para borrar un juego de la cola desde el backend antiguo ***/
function deleteGameQueueMYSQL(req, res) {

  QueueGame.findOne({
    id: req.body.queueid
  }, function(err, qgame) {
    if (!qgame) {
      console.log("No qGame found.");
      var message = '404 : qGame not exists ' + req.body.queueid;
      return NotificationsCtrl.newAlert(message, res);
    } else {
      console.log(qgame);
      res.send(200, qgame);
    }

  });
};

/*** Metodo para obtener y actualizar el pricio con el SKU de CeX ***/
function getPriceCex(req, res, callback) {

  var game = req.body.game;
  const Nightmare = require('nightmare');

  const nightmare = Nightmare({
    show: false,
    executionTimeout: 10000,
    gotoTimeout: 10000,
    loadTimeout: 10000,
    waitTimeout: 10000
  });

  var url = 'https://es.webuy.com/product-detail/?id=';
  var selector = '.sellPrice';
  if (!game.sku_cex) {
    console.log("El juego " + game.name + " (" + game.id + ") no está disponible en CeX.");
    callback();

    // skus.shift();
    // getPriceCex(skus);
  } else {
    console.log("CeX: " + game.name + " (" + game.id + ") con SKU " + game.sku_cex);
    nightmare
      .goto(url + game.sku_cex)
      .wait(selector)
      .evaluate(selector => {
        return {
          price: document.querySelector(selector).innerText
        };
      }, selector)
      .then(extracted => {
        if (extracted.price.replace('€', '') != game.price_cex) {
          console.log("\tPrecio: " + extracted.price.replace('€', ''));
          Game.findById(game._id, function(err, game) {
            if (err) return res.send(500, err + game.name);
            if (!game) return res.send(404, 'Game not found');

            game.price_cex = extracted.price.replace('€', '');
            game.save(function(err, game) {
              if (err) return res.send(500, err + game.name);
              callback();
            });
          });

        } else {
          //bot.telegram.sendMessage(-379097289, "El precio de " + game.name + " ya está actualizado")
          console.log("El precio de " + game.name + " ya está actualizado")
          callback();
        }
      })
      .catch(function(error) {
        console.log('Retrieve price failed:', error);
        callback();
      });
  }
}

function updatePrices(req, res) {
  Game.aggregate([{
        $lookup: {
          from: "queuegames",
          localField: "queue",
          foreignField: "_id",
          as: "queue"
        }
      },
      {
        $lookup: {
          from: "genres",
          localField: "genres",
          foreignField: "_id",
          as: "genres"
        }
      },
      {
        $match: {
          'stock': {
            $gt: 0
          },
          "relased": 1
        }
      },
      {
        $sort: {
          "name": 1
        }
      },

      {
        $limit: 5
      },
      {
        "$project": {
          "name": 1,
          "id": 1,
          "cover": 1,
          "screenshot": 1,
          "summary": 1,
          "pegi": {
            '$toInt': '$pegi'
          },
          "summary": 1,
          "genres": 1,
          "sku_game": 1,
          "sku_cex": 1,

          "price_game": {
            $let: {
              vars: {
                'priceD': {
                  $multiply: ["$price_game", discount]
                },
              },
              in: {
                $divide: [{
                  $trunc: {
                    $multiply: ["$$priceD", 100]
                  }
                }, 100]
              }
            },
          },
          "booking": 1,
          "releaseDate": 1,
          "queueNum": {
            "$size": {
              "$filter": {
                "input": "$queue",
                "as": "que",
                "cond": {
                  $and: [{
                      $eq: ["$$que.deleted", false]
                    },
                    {
                      $eq: ["$$que.requested", false]
                    }
                  ]
                }
              }
            }
          },
          "queue": {
            "$subtract": ["$stock", {
              "$size": {
                "$filter": {
                  "input": "$queue",
                  "as": "que",
                  "cond": {
                    $and: [{
                        $eq: ["$$que.deleted", false]
                      },
                      {
                        $eq: ["$$que.requested", false]
                      }
                    ]
                  }
                }
              }
            }]
          }
        },
      }
    ],
    function(err, games) {
      if (err) res.send(500, err.message);
      async.forEach(games, function(game, callback) {
          // if (game.price_game == undefined) {
          //   console.log(game.name + ' has not price.');
          //   callback();
          // }

          if (!game.sku_cex) {
            console.log("El juego " + game.name + " (" + game.id + ") no está disponible en CeX.");

          } else {
            console.log("CeX: " + game.name + " (" + game.id + ") con SKU " + game.sku_cex);
            // var price =
            // async getPresiu => {
            //console.log(game);
            const nightmare = Nightmare({
              show: false,
              executionTimeout: 100000,
              gotoTimeout: 100000,
              loadTimeout: 100000,
              waitTimeout: 100000
            });

            var url = 'https://es.webuy.com/product-detail/?id=';
            var selector = '.sellPrice';
            console.log(url + game.sku_cex);

            const result = nightmare
              .goto(url + game.sku_cex)
              .wait(selector)
              .evaluate(selector => {
                return {
                  price: document.querySelector(selector).innerText
                };
              }, selector)

              .then(extracted => {
                if (extracted.price.replace('€', '') != game.price_cex) {
                  console.log("\tPrecio: " + extracted.price.replace('€', ''));
                  Game.findById(game._id, function(err, game) {
                    if (err) return res.send(500, err + game.name);
                    if (!game) return res.send(404, 'Game not found');

                    game.price_cex = extracted.price.replace('€', '');
                    game.save(function(err, game) {
                      if (err) return res.send(500, err + game.name);
                      //callback();
                    });
                  });
                } else {
                  //bot.telegram.sendMessage(-379097289, "El precio de " + game.name + " ya está actualizado")
                  console.log("El precio de " + game.name + " ya está actualizado")

                }
              }).catch(error => {
                console.log(error);
              });

          }


        },
        function(err) {
          console.log('iterating done');
        });
    });
}

async function getPrice(game) {

  const nightmare = Nightmare({
    show: false,
    executionTimeout: 10000,
    gotoTimeout: 10000,
    loadTimeout: 10000,
    waitTimeout: 10000
  });

  var url = 'https://es.webuy.com/product-detail/?id=';
  var selector = '.sellPrice';

  await nightmare
    .goto(url + game.sku_cex)
    .wait(selector)
    .evaluate(selector => {
      return {
        price: document.querySelector(selector).innerText
      };
    }, selector)
    .then(extracted => {
      if (extracted.price.replace('€', '') != game.price_cex) {
        console.log("\tPrecio: " + extracted.price.replace('€', ''));
        Game.findById(game._id, function(err, game) {
          if (err) return res.send(500, err + game.name);
          if (!game) return res.send(404, 'Game not found');

          game.price_cex = extracted.price.replace('€', '');
          game.save(function(err, game) {
            if (err) return res.send(500, err + game.name);
            callback();
          });
        });
      } else {
        //bot.telegram.sendMessage(-379097289, "El precio de " + game.name + " ya está actualizado")
        console.log("El precio de " + game.name + " ya está actualizado")

      }
    })
    .catch(function(error) {
      console.log('Retrieve price failed:', error);
      //callback();
    });
}


module.exports = {
  findAllGames,
  findByName,
  findPremieres,
  findMain,
  findByGenre,
  findById,
  saveGame,
  updateGame,
  deleteGame,
  addGameQueue,
  moveQueue,
  addGameQueueMYSQL,
  deleteGameQueueMYSQL,
  findGamesStock,
  buyGame,
  updatePrices
  //getPriceCex
}
