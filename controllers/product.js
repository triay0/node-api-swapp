var sk_stripe_test = 'sk_test_E1SdK829cuZtucs5qI5dwQoU';
var sk_stripe_live = 'sk_live_guvFbTPzk0ZWZry6Io7JAddv';
var stripe = require("stripe")(sk_stripe_live);
var mongoose = require('mongoose');
var User = require('../models/user');
var async = require('async');
var NotificationsCtrl = require('../controllers/notifications');

/*** Metodo para mostrar los packs ***/
function getProducts(req, res) {
  var packs = [];


  stripe.products.list({
      active: true,
      type: 'good'
    },
    function(err, products) {

      async.forEach(products.data, function(product, callback) {

          stripe.skus.retrieve(product.attributes[0], function(err, sku) {

            var pack = {
              id: product.attributes[0],
              name: product.name,
              months: parseInt(product.attributes[2]),
              plan: product.attributes[3],
              caption: product.caption,
              description: product.description,
              price: (sku.price / 100).toString() + '€',
              credit: product.attributes[1]
            };
            packs.push(pack);
            callback();
          });
        },
        function(err) {
          packs.sort(function(a, b) {

            if (a.plan == 'Basic' && b.plan == 'Pro') {
              return -1;
            } else if (a.plan == 'Pro' && b.plan == 'Basic') {
              return 1;
            } else {
              if (a.months > b.months) {
                return 1;
              } else {
                return -1;
              }
            }
          });
          res.status(200).jsonp(packs);
        });
    });
}

/*** Metodo para comprar un pack ***/
function buyProduct(req, res) {
  var params = req.body;
  User.findOne({
      uid: req.user.uid
    })
    .exec(function(err, user) {
      if (err) return res.send(500, err.message);
      if (!user) {
        return res.send(404, 'El usuario no existe.');
      } else {
        stripe.orders.create({
          currency: 'eur',
          items: [{
            type: 'sku',
            parent: params.item
          }],

          customer: user.stripe_id
        }, function(err, order) {
          if (err) return res.send(500, err);
          stripe.orders.pay(
            order.id, {
              customer: user.stripe_id, // obtained with Stripe.js
            },
            function(err, order) {
              if (err) return res.send(500, err);
               res.send(200, order);
               var message = 'SALE: NEW PACK';
               return NotificationsCtrl.newAlert(message);
            }
          );
        });

      }
    });
}


module.exports = {
  getProducts,
  buyProduct
}
