var mongoose = require('mongoose');
var sk_stripe_test = 'sk_test_E1SdK829cuZtucs5qI5dwQoU';
var sk_stripe_live = 'sk_live_guvFbTPzk0ZWZry6Io7JAddv';

var stripe = require("stripe")(sk_stripe_live);
var moment = require('moment');
var Order = require('../models/order');
var Game = require('../models/game');
var User = require('../models/user');
var QueueGame = require('../models/queueGame');
var async = require('async');
var NotificationsCtrl = require('../controllers/notifications');


/*** Metodo para guardar un pedido ***/
function saveOrder(req, res) {

  var params = req.body;
  var id;

  User.findOne({
      uid: req.user.uid
    })
    .exec(function(err, user) {
      if (err) return res.send(500, err.message);
      if (!user) {
        return res.send(404, 'El usuario no existe.');
      } else {
        var order = new Order();
        order.user = user.id;
        order.name = params.name;
        order.phone = params.phone;
        order.gameIn = params.gameIn;
        order.address.postal1 = params.postal1;
        order.address.number = params.number;
        order.address.postal2 = params.postal2;
        order.address.province = params.province;
        order.address.zipcode = params.zipcode;
        order.address.city = params.city;

        if (params.gameOut) {
          order.gameOut = params.gameOut;
        }
        order.comment = params.comment;
        order.createdAt = moment().format();
        order.statusFlutter = 0;
        order.status = 1;
        order.platform = params.platform;
        order.timeZone = params.timeZone;

        order.save(function(err, order) {
          if (err) return res.send(500, err);
          if (!order) return res.status(404).send({
            message: 'El pedido no ha sido guardado'

          });
          else {
            user.orders.push(order._id);
            user.save(function(err) {
              if (err) return res.send(500, err);
              //
              //  console.log("gameOut lenght: " + order.gameOut.length);
              for (var game = 0; game < order.gameIn.length; game++) {
                console.log("game" + order.gameIn[game]);
                QueueGame.findOne({
                  $and: [{
                      game: order.gameIn[game]
                    },
                    {
                      user: user.id
                    },
                    {
                      requested: false
                    },
                    {
                      deleted: false
                    }
                  ]
                }, function(err, qgame) {
                  if (err) return res.send(500, err);
                  console.log(qgame);
                  qgame.requested = true;
                  //
                  qgame.save(function(err) {
                    if (err) return res.send(500, err.message);
                    //res.status(200).jsonp(qgame);
                  });
                });
              }


              res.status(200).jsonp(user);
            });
          }

        });
        //return res.status(200).jsonp(user);
      }
    });
};

/*** Metodo para modificar un pedido ***/
function updateOrder(req, res) {
  var orderId = req.params.id;
  var update = req.body;


//PERMISOS (Si el pedido es del usuario o el admin)
  // console.log(req.user);
  //
  // if (req.user.sub != update.user && req.user.sub != 'admin') {
  //   return res.status(403).send({
  //     message: 'No tienes permisos para actualizar el pedido.'
  //   });
  // }

  Order.findByIdAndUpdate(
    orderId, update, {
      new: true // new:true para que devuelva el objeto actualizado
    },
    function(err, orderUpdated) {
      if (err) return res.status(500).send({
        message: 'Error en la peticion. Error: ' + err.message
      });
      if (!orderUpdated) {
        return res.status(404).send({
          message: 'No se ha podido actualizar el pedido.'
        });
      }
      return res.status(200).send(orderUpdated);

    });
}

/*** Metodo para cancelar un pedido ***/
function cancelOrder(req, res) {

  Order.findById(
    req.params.id,
    function(err, order) {
      if (err) return res.send(500, err.message);
      if (!order) {
        return res.send(404, 'Order not found.');
      }
      order.statusFlutter = -1,
        order.status = -1,

        order.save(function(err) {
          if (err) return res.send(500, err.message);
          res.status(200).jsonp(order);
        });
    });
}

/*** Metodo para ver un pedido ***/
function getOrder(req, res) {

  Order.findById(
      req.params.id,
    ).populate({
      path: 'gameIn',

      select: 'id name cover'

    })
    .populate({
      path: 'gameOut',
      select: 'id name cover'
    })
    .exec(function(err, order) {
      if (err) return res.send(500, err.message);
      if (!order) {
        return res.send(404, 'Order not found.');
      } else {
        return res.status(200).jsonp(order);
      }
    });
}

/*** Metodo para guardar el numero de Seguimiento de un pedido ***/
function setTracking(req, res) {

  Order.findById(
    req.params.id,
    function(err, order) {
      if (err) return res.send(500, err.message);
      if (!order) {
        return res.send(404, 'Order not found.');
      }
      order.trackingIn = req.body.tracking,

        order.save(function(err) {
          if (err) return res.send(500, err.message);
          res.status(200).jsonp(order);
        });
    });
}

/*** Metodo para enviar un pedido ***/
function sendOrder(req, res) {

  Order.findById(
    req.params.id,
    function(err, order) {
      if (err) return res.send(500, err.message);
      if (!order) {
        return res.send(404, 'Order not found.');
      }
      order.status = 2;
      order.statusFlutter = 4;

      order.save(function(err) {
        if (err) return res.send(500, err.message);
        res.status(200).jsonp(order);
      });
    });
}

/*** Metodo para guardar un pedido desde el backend antiguo ***/
function createOrderMysql(req, res) {
  let params = req.body;

  async.parallel({
      gameIn1: function(in1) {
        Game.findOne({
            id: params.gameIn1
          })

          .exec(in1);
      },
      gameIn2: function(in2) {
        Game.findOne({
            id: params.gameIn2
          })

          .exec(in2);
      },
      gameOut1: function(out1) {
        Game.findOne({
            id: params.gameOut1
          })

          .exec(out1);
      },
      gameOut2: function(out2) {
        Game.findOne({
            id: params.gameOut2
          })

          .exec(out2);
      },
      user: function(cb2) {
        User.findOne({
          id: parseInt(params.user_id)
        }).exec(cb2);
      }

    },
    function(err, result) {
      //console.log("User: " + result.user._id);
      //console.log("GameOut: " + result.gameOut1._id);
      var gameIn = [];
      var gameOut = [];
      if (result.gameIn1 != null) {
        gameIn.push(result.gameIn1._id);
      }
      if (result.gameIn2 != null) {
        gameIn.push(result.gameIn2._id);
      }
      if (result.gameOut1 != null) {
        gameOut.push(result.gameOut1._id);
      }
      if (result.gameOut2 != null) {
        gameOut.push(result.gameOut2._id);
      }
      console.log("Game1: " + result.gameIn1._id);


      if (err) res.status(400).jsonp(err);
      if (result.user == null) {
        var message = '404 : User not exists ' + params.user_id;
        return NotificationsCtrl.newAlert(message);
      }
      if (result.gameIn1 == null) {
        var message = '404 : Game not exists ' + params.game_id;
        return NotificationsCtrl.newAlert(message, res);
      }

      var order = new Order();
      order.user = result.user._id;
      order.name = params.name;
      order.phone = params.phone;
      order.gameIn = gameIn;
      order.address.postal1 = params.postal1;
      order.address.number = params.number;
      order.address.postal2 = params.postal2;
      order.address.province = params.province;
      order.address.zipcode = params.zipcode;
      order.address.city = params.city;
      order.statusFlutter = 0;
      order.gameOut = gameOut;
      order.comment = params.comment;
      order.createdAt = moment().format();
      order.status = 1;
      order.platform = params.platform;
      order.timeZone = params.timeZ;


      order.save(function(err, order) {
        if (err) return res.send(500, err);
        return res.status(200).jsonp(order);
        // Game.findOne({
        //     id: params.game_id
        //   }),
        //   function(err, game) {
        //     game.queue.push(doc._id);
        //     game.save(function(err) {
        //       if (err) return res.send(500, err + game.name);
        //       return res.status(200).jsonp(qGame);
        //     });
        //   };

      });
    });
}

/*** Metodo para recibir el pedido de vuelta y actualizar el stock ***/
function orderRecieved(req, res) {

  Order.findById(
    req.params.id,
    function(err, order) {
      if (err) return res.send(500, err.message);
      if (!order) {
        return res.send(404, 'Order not found.');
      }
      order.status = 0;
      order.statusFlutter = 6;

      order.save(function(err) {
        if (err) return res.send(500, err.message);

        Game.find({
            _id: order.gameOut
          },
          function(err, games) {
            if (err) console.log(err);
            for (var i = 0; i < games.length; i++) {

              Game.findById(games[i]._id, function(err, game) {
                game.stock = game.stock + 1;
                game.save();
              });
            }
          });

      });
    });
}

module.exports = {
  saveOrder,
  updateOrder,
  getOrder,
  cancelOrder,
  setTracking,
  orderRecieved,
  createOrderMysql,
  sendOrder
}
