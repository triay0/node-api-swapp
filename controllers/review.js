var Review = require('../models/review');

/*** Metodo para añadir una reseña ***/
function saveReview(req, res) {
  var id;

  var user = User.findOne({
    uid: req.user.uid
  }, function(err, user) {
    if (err) return res.send(500, err.message);
    if (!user) {
      return res.send(404, 'El usuario no existe.');
    } else {
      id = user._id;

      var review = new Review({
        user: user._id,
        game: req.params.id,
        rating: req.body.rating,
        comment: req.body.comment,
        platform: req.body.platform,
        createdAt: moment(),
      });

      review.save(function(err) {
        if (err) return res.send(500, err);
        res.status(200).jsonp(review);
      });
    }
  });
}

/*** Metodo para obtener las reseñas de un juego ***/
function getReviews(req, res) {

  var reviews = Review.find({
      game: req.params.id
    })
    .select('rating comment createdAt')
    .populate({
      path: 'user',
      select: 'nick'
    })
    .exec(function(err, reviews) {
      if (err) return res.send(500, err.message);
      if (!reviews) {
        return res.send(404, 'El reviews no existe.');
      } else {
        res.status(200).jsonp(reviews);
      }
    });
}

module.exports ={
  saveReview,
  getReviews,
}
