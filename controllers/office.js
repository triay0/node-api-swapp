'use strict'
var mongoose = require('mongoose');
var Office = require('../models/office');
var moment = require('moment');
const utf8 = require('utf8');
var soap = require('soap');
var request = require("request");

var Order = require('../models/order');

var NotificationsCtrl = require('../controllers/notifications');
const fs = require('fs');


function getTrackingCorreos(req, res) {

  var url = 'https://online.correos.es/ServiciosWebLocalizacionMI/LocalizacionMI.asmx?wsdl';
  let tracking = req.params.tracking;
  var xml = '<?xml version="1.0" encoding="utf-8" ?><ConsultaXMLin Idioma="1" xmlns:xsd="http://www.w3.org/2001/XMLSchema" xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance"><Consulta><Codigo>PL5TWC0710001830128025B</Codigo></Consulta></ConsultaXMLin>';

  var args = {
    XMLin: xml
  };

  soap.createClient(url, function(err, client) {
    client.setEndpoint('https://online.correos.es/ServiciosWebLocalizacionMI/LocalizacionMI.asmx');
    client.ConsultaLocalizacionEnviosFasesMasivo(args, function(err, result) {
      return res.status(200).jsonp(client.lastRequest);
      var parseString = require('xml2js').parseString;
      parseString(result.ConsultaLocalizacionEnviosFasesMasivoResult, function(err, result) {
        var data = result['ConsultaXMLout']['Respuestas'][0]['DatosIdiomas'][0]['DatosEnvios'][0]['Datos'];
        res.status(200).jsonp(data);
      });
    });
  });
};

async function getTrackingMRW(req, res) {
  var url = 'https://clientes.mrw.es:4433/TrackingService.svc?wsdl';
  let tracking = req.params.tracking;
  let orderId = req.params._id;
  let out = req.params.out;
  let status = req.params.status;
  let user = req.params.user;
  var type;

  var args = {
    "tem:login": "4320SGSWAPPDREMS",
    "tem:pass": "4320SGSWAPPDREMS",
    "tem:codigoIdioma": 3082,
    "tem:tipoFiltro": 0,
    "tem:valorFiltroDesde": tracking,
    "tem:valorFiltroHasta": tracking,
    "tem:tipoInformacion": 0
  };

  var wsdlOptions = {
    envelopeKey: 'soapenv',
    returnFault: true,
  };

  var client = await soap.createClientAsync(url, wsdlOptions);

  return new Promise(function(resolve, reject) {
    client.GetEnvios(args, function(err, result) {
      if (err) {
        console.log('Algo fue mal.');
        reject(err);
      }
      if (result['GetEnviosResult']['Seguimiento'] == undefined) {
        reject('Order not found ' + orderId);
      }
      var resultShort = result['GetEnviosResult']['Seguimiento']['Abonado']['SeguimientoAbonado']['Seguimiento'];

      Order.findById(
        orderId,
        function(err, order) {
          if (err) return res.send(500, err.message);
          if (!order) {
            return res.send(404, 'Order not found.');
          }
          var statusMYSQL;
          var statusFlutter;
          switch (resultShort['Estado']) {
            case "01":
            case "02":
            case "03":
            case "04":
            case "05":
            case "06":
            case "05":
            case "08":
            case "09":
            case "10":
            case "12":
            case "18":
            case "19":
            case "31":
            case "32":
            case "35":
            case "41":
            case "42":
            case "43":
            case "44":
            case "45":
            case "60":
            case "61":
            case "76":
            case "91":
            case "92":
            case "93":
            case "94":
            case "95":
              type = 3;
              statusMYSQL = 2;
              statusFlutter = 5;
              order.deliveryErrorMessage = resultShort['EstadoDescripcion'];
              break;
            case "47":
            case "48":
            case "49":
              type = 2;
              statusMYSQL = 1;
              statusFlutter = 3;
              break;
            case "17":
            case "72":
            case "13":
            case "33":
              type = 3;
              order.expectedDeliveryDate = getNextBussinessDay();
              statusMYSQL = 2;
              statusFlutter = 4;
              break;
            case "16": //Reparto
            case "07":
            case "79":
              type = 3;
              statusMYSQL = 2;
              statusFlutter = 5;
              break;
            case "00":
              type = 4;
              if (!out) {

                statusMYSQL = 0;
                statusFlutter = 6;
              } else {
                statusMYSQL = 4;
                statusFlutter = 5;
              }

              break;
            case "57":
              type = 3;
              statusMYSQL = 2;
              statusFlutter = 4;
              break;
            default:
              console.log("Status not defined: " + resultShort['Estado'] + resultShort['EstadoDescripcion']);
              break;
          }
          order.statusFlutter = statusFlutter;
          order.status = statusMYSQL;
          order.statusMessage = resultShort['EstadoDescripcion'] + " " + resultShort['Publicado'];

          var message = tracking + ' : ' + order.statusMessage + '\n';

          if (statusFlutter !== status) {
            fs.appendFile("./tmp/updateMRW.txt", message, function(err) {
              if (err) {
                console.log(err);
              }
            });
            order.save(function(err) {
              if (err) res.status(500).send(err.message + order);
              console.log("Order  updated " + orderId);
              var req = [];
              req.body.user = user;
              req.body.type = type;
              NotificationsCtrl.pushNotification(req, res);
              resolve(resultShort);
            });
          } else {
            resolve(resultShort);
          }
        });
    }, {
      postProcess: function(_xml) {
        _xml = _xml.replace('xmlns:xsi=\"http://www.w3.org/2001/XMLSchema-instance\"', '');
        _xml = _xml.replace('tns', 'tem');
        _xml = _xml.replace('xmlns:wsu=\"http://docs.oasis-open.org/wss/2004/01/oasis-200401-wss-wssecurity-utility-1.0.xsd\"', '');
        _xml = _xml.replace('xmlns=\"http://tempuri.org/\"', '');
        _xml = _xml.replace('xmlns:msc=\"http://schemas.microsoft.com/ws/2005/12/wsdl/contract\"', '');
        _xml = _xml.replace('GetEnvios ', 'tem:GetEnvios');
        _xml = _xml.replace('</GetEnvios', '</tem:GetEnvios');
        return _xml;
      }
    });
  });
}


function setTracking(req, res) {
  var soap = require('soap');
  var urlTest = 'http://sagec-test.mrw.es/MRWEnvio.asmx?WSDL';
  var urlPro = 'http://sagec.mrw.es/MRWEnvio.asmx?WSDL';

  var today = moment(new Date()).format("DD/MM/YYYY");

  var auth = {
    "mrw:AuthInfo": {
      "mrw:CodigoFranquicia": '04320',
      "mrw:CodigoAbonado": '001072',
      "mrw:UserName": '4320SGSWAPPDREMS',
      "mrw:Password": '4320SGSWAPPDREMS'
    }
  };

  var codService = "0200";
  var weight = "2";

  if (req.body.gameOut !== "" && req.body.gameOut != false) { //Has game out
    codService = "0810";
    if (!req.body.zipcode.includes("46")) {
      weight = "5";
    }

  }

  //FALTA RETORNO
  var args = {
    "mrw:request": {
      "mrw:DatosEntrega": {
        "mrw:Direccion": {
          "mrw:Via": req.body.postal1,
          "mrw:Numero": req.body.num,
          "mrw:Resto": req.body.postal2,
          "mrw:CodigoPostal": req.body.zipcode,
          "mrw:Poblacion": req.body.province,
        },
        "mrw:Nombre": req.body.name,
        "mrw:Telefono": req.body.phone,
        // Horario:{
        //   HorarioRangoRequest:{
        //     Desde:,
        //     Hasta:,
        //   }
        // },
        "mrw:Observaciones": req.body.observations
      },
      "mrw:DatosServicio": {
        "mrw:Fecha": today,
        "mrw:Referencia": req.body.gameIn, //Games IN
        "mrw:CodigoServicio": codService, //Canje 0810
        "mrw:NumeroBultos": '1',
        "mrw:Peso": weight,
        "mrw:EntregaPartirDe": req.body.from

      }
    }
  };

  var wsdlOptions = {
    ignoredNamespaces: true,
    returnFault: true,
  };

  soap.createClient(urlPro, wsdlOptions, function(err, client) {
    client.addSoapHeader(auth);

    client.TransmEnvio(args, function(err, result) {
      if (err) return res.status(500).jsonp(err);

      return res.status(200).jsonp(result);
    }, {
      postProcess: function(_xml) {
        _xml = _xml.replace('xmlns:xsi=\"http://www.w3.org/2001/XMLSchema-instance\"', '');
        _xml = _xml.replace('xmlns:tm=\"http://microsoft.com/wsdl/mime/textMatching/\"', '');
        _xml = _xml.replace('xmlns=\"http://www.mrw.es/\"', '');
        _xml = _xml.replace('tns', 'mrw');
        _xml = _xml.replace('TransmEnvio', 'mrw:TransmEnvio');
        _xml = _xml.replace('</TransmEnvio', '</mrw:TransmEnvio');
        return _xml;
      }
    });
  });
}


function trackOrder(req, res) {

}


function getStatusOrder(req, res) {
  var soap = require('soap');
  var url = 'http://sagec-test.mrw.es/MRWEnvio.asmx?WSDL';

  var args = {
    "tem:GetEnvios": {
      "tem:login": req.body.postal1,
      "tem:pass": req.body.num,
      "tem:codigoIdioma": 3082,
      "tem:tipoFiltro": req.body.postal2,
      //"tem:CodigoPostal": req.body.zipcode,
      "tem:TipoInformacion": req.body.province,
    },


  };

}

function findByZipcode(req, res) {

  var soap = require('soap');
  var url = 'http://localizadoroficinas.correos.es/localizadoroficinas/wsdl/localizadorMAUO.wsdl';
  var args = {
    codigoPostal: req.params.zipcode
  };
  soap.createClient(url, function(err, client) {
    var offices = [];

    client.setEndpoint('http://localizadoroficinas.correos.es/localizadoroficinas');
    client.procesaLocalizador(args, function(err, result) {
      for (var i = 0, len = result.arrayOficina.item.length; i < len; i++) {
        var office = {
          id: parseInt(result.arrayOficina.item[i].unidad),
          name: result.arrayOficina.item[i].nombre,
          zipcode: result.arrayOficina.item[i].cp,
          location: result.arrayOficina.item[i].descLocalidad,
          phone: parseInt(result.arrayOficina.item[i].telefono),
          scheduleMF: result.arrayOficina.item[i].horarioLV,
          scheduleS: result.arrayOficina.item[i].horarioS,
          scheduleH: result.arrayOficina.item[i].horarioF,
          lat: parseFloat(result.arrayOficina.item[i].latitudETRS89),
          long: parseFloat(result.arrayOficina.item[i].longitudETRS89)
        };
        offices.push(office);
      }
      res.status(200).jsonp(offices);
    });
  });
};


function getNextBussinessDay() {
  var businessDay;

  var today = moment().day();
  if (today === 5) { // friday, show monday
    // set to monday
    businessDay = moment().weekday(8);
  } else if (today === 6) { // saturday, show monday
    // set to monday
    businessDay = moment().weekday(8);
  } else { // other days, show next day
    businessDay = moment().add('days', 1);
  }
  return businessDay;
}

module.exports = {
  findByZipcode,
  getTrackingCorreos,
  getTrackingMRW,
  setTracking,
  getStatusOrder
}
