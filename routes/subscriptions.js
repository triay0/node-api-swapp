'use strict'
var express = require("express");
var SubscriptionsCtrl = require('../controllers/subscriptions');

var api = express.Router();
var md_auth = require('../middlewares/authenticated');

api.post('/users/subscription/',  md_auth.isAuthenticated, SubscriptionsCtrl.createSubscription);
api.put('/users/subscription/cancel/',  md_auth.isAuthenticated, SubscriptionsCtrl.cancelSubscription);
api.put('/users/subscription/cancel/reason',  md_auth.isAuthenticated, SubscriptionsCtrl.cancelReason);
api.put('/users/subscription/reactivate/',  md_auth.isAuthenticated, SubscriptionsCtrl.reactivateSubscription);
api.put('/users/subscription/activate/',  md_auth.isAuthenticated, SubscriptionsCtrl.activateSubscription);
api.put('/subscriptions/:id',  SubscriptionsCtrl.updateSubscription);
//Get Orders by user

module.exports = api;
