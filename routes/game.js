'use strict'
var express = require("express");
var GameCtrl = require('../controllers/game');

var api = express.Router();
var md_auth = require('../middlewares/authenticated');

api.get('/games', GameCtrl.findAllGames);
api.post('/games', GameCtrl.saveGame);
api.get('/games/premieres/', GameCtrl.findPremieres);
api.get('/games/main/', GameCtrl.findMain);
api.get('/games/name/:id', GameCtrl.findByName);
api.get('/games/genre/:id', GameCtrl.findByGenre);
api.get('/games/stock', GameCtrl.findGamesStock);
api.get('/games/updatePrices', md_auth.isAdmin, GameCtrl.updatePrices);
api.post('/games/buy', md_auth.isAuthenticated, GameCtrl.buyGame);
api.get('/games/:id', GameCtrl.findById);
api.put('/games/:id', GameCtrl.updateGame);
api.delete('/games/:id', GameCtrl.deleteGame);
api.post('/games/:id/add',  md_auth.isAuthenticated, GameCtrl.addGameQueue);
api.post('/games/add/mysql',   GameCtrl.addGameQueueMYSQL);
api.delete('/games/delete/mysql',   GameCtrl.deleteGameQueueMYSQL);

api.get('/queue', GameCtrl.moveQueue);

module.exports = api;
