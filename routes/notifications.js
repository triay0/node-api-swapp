'use strict'
var express = require("express");
var NotificationCtrl = require('../controllers/notifications');

var api = express.Router();
//var md_auth = require('../middlewares/authenticated');

api.get('/mails',  NotificationCtrl.newAlert);
api.get('/mails2',  NotificationCtrl.pushNotification);
// api.get('/sendgrid', NotificationCtrl.sendgridTest);

//Get Orders by user

module.exports = api;
