'use strict'
var express = require("express");
var GenreCtrl = require('../controllers/genre');

var api = express.Router();
var md_auth = require('../middlewares/authenticated');

api.get('/genre', GenreCtrl.getGenres);
api.post('/genre', GenreCtrl.saveGenre);

module.exports = api;
