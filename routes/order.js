'use strict'
var express = require("express");
var OrderCtrl = require('../controllers/order');

var api = express.Router();
var md_auth = require('../middlewares/authenticated');

api.post('/order', md_auth.isAuthenticated, OrderCtrl.saveOrder);
api.get('/order/:id', md_auth.isAdmin, OrderCtrl.getOrder);
api.put('/order/:id', md_auth.isAuthenticated, OrderCtrl.updateOrder);
api.post('/order/mysql', OrderCtrl.createOrderMysql);
api.put('/order/:id/tracking/', md_auth.isAdmin, OrderCtrl.setTracking);
api.delete('/order/:id/cancel', md_auth.isAuthenticated, OrderCtrl.cancelOrder);
api.put('/order/:id/recieved', md_auth.isAdmin, OrderCtrl.orderRecieved);
api.put('/order/:id/send', md_auth.isAdmin, OrderCtrl.sendOrder);

//Get Orders by user

module.exports = api;
