'use strict'
var express = require("express");
var OfficeCtrl = require('../controllers/office');

var api = express.Router();

api.get('/offices/:zipcode',  OfficeCtrl.findByZipcode);
api.get('/tracking/:tracking/correos/',  OfficeCtrl.getTrackingCorreos);
api.get('/tracking/:tracking/mrw/',  OfficeCtrl.getTrackingMRW);
api.post('/tracking/tracking',  OfficeCtrl.setTracking);

module.exports = api;
