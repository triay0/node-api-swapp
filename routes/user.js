'use strict'
var express = require("express");
var UsersCtrl = require('../controllers/user');

var api = express.Router();
var md_auth = require('../middlewares/authenticated');

api.post('/users', UsersCtrl.saveUser);
api.get('/users/me', md_auth.isAuthenticated, UsersCtrl.getUser);
api.put('/users/me',  md_auth.isAuthenticated, UsersCtrl.updateUser);
api.get('/users/queue',  md_auth.isAuthenticated, UsersCtrl.getQueue);
api.get('/users/orders/', md_auth.isAuthenticated, UsersCtrl.getOrders);
api.get('/users/status', md_auth.isAuthenticated, UsersCtrl.checkSubscription);
api.get('/users/games/', md_auth.isAuthenticated, UsersCtrl.getGamesAtHome);
api.get('/users/card/',  md_auth.isAuthenticated, UsersCtrl.hasCard);
api.get('/users/tokenAuth/', UsersCtrl.createToken);
api.get('/users/:id',  md_auth.isAdmin, UsersCtrl.getUserById);
api.get('/users/search/:id',  md_auth.isAdmin, UsersCtrl.findUser);
api.post('/users/card/',  md_auth.isAuthenticated, UsersCtrl.saveCard);
api.delete('/users/queue/:id',  md_auth.isAuthenticated, UsersCtrl.deleteQueue);

module.exports = api;
