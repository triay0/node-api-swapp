'use strict'
var express = require("express");
var ReviewCtrl = require('../controllers/review');

var api = express.Router();
var md_auth = require('../middlewares/authenticated');

api.post('/games/:id/review', md_auth.isAuthenticated, ReviewCtrl.saveReview);
api.get('/games/:id/review', ReviewCtrl.getReviews);

module.exports = api;
