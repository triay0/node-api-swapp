'use strict'
var express = require("express");
var AdminCtrl = require('../controllers/admin');

var api = express.Router();
var md_auth = require('../middlewares/authenticated');

api.get('/admin/orders/', md_auth.isAdmin, AdminCtrl.getPendingOrders);
api.get('/admin/orders/MRW', md_auth.isAuthenticated, AdminCtrl.getPendingOrdersMRW);
api.get('/admin/users/:id', md_auth.isAdmin, AdminCtrl.searchUser);
api.get('/admin/users/:id/orders/', md_auth.isAdmin, AdminCtrl.getOrders);
api.get('/admin/users/:id/home/', md_auth.isAdmin, AdminCtrl.getHome);
api.get('/admin/users/:id/queue/', md_auth.isAdmin, AdminCtrl.getQueue);
api.put('/admin/users/:id',  md_auth.isAdmin, AdminCtrl.updateUser);

module.exports = api;
