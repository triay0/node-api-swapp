'use strict'
var express = require("express");
var ProductCtrl = require('../controllers/product');
var StripeCtrl = require('../controllers/stripe');
var api = express.Router();
var md_auth = require('../middlewares/authenticated');


api.get('/products',md_auth.isAuthenticated,  ProductCtrl.getProducts);
api.post('/products/buy',  md_auth.isAuthenticated, ProductCtrl.buyProduct);
api.get('/stripe/active',  md_auth.isAdmin, StripeCtrl.getActiveSubscriptions);

module.exports = api;
