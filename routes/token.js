'use strict'
var express = require("express");
var TokenCtrl = require('../controllers/token');

var api = express.Router();

var md_auth = require('../middlewares/authenticated');


api.post('/users/token/',  md_auth.isAuthenticated, TokenCtrl.addtoken);
api.put('/users/token/',  md_auth.isAuthenticated, TokenCtrl.invalidateToken);
api.get('/users/token/',  md_auth.isAuthenticated, TokenCtrl.getTokenFCM);

module.exports = api;
