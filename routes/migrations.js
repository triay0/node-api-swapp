'use strict'
var express = require("express");
var MigrationsCtrl = require('../controllers/migrations');

var api = express.Router();
var md_auth = require('../middlewares/authenticated');

api.get('/migrations', md_auth.isAdmin, MigrationsCtrl.migrateQueue);
api.get('/migrations/orders', md_auth.isAdmin, MigrationsCtrl.migrateOrders);
api.get('/migrations/orders/addresses', md_auth.isAdmin, MigrationsCtrl.migrateAdressesOrder);
api.get('/migrations/orders/games1', md_auth.isAdmin, MigrationsCtrl.migrateOrdersGame1);
api.get('/migrations/orders/games2', md_auth.isAdmin, MigrationsCtrl.migrateOrdersGame2);
api.get('/migrations/subs/', md_auth.isAdmin, MigrationsCtrl.migrateSubscriptions);
api.get('/migrations/addresses/', md_auth.isAdmin, MigrationsCtrl.migrateAdresses);
api.get('/migrations/users/game1', md_auth.isAdmin, MigrationsCtrl.migrateUserGame1);
api.get('/migrations/users/game2', md_auth.isAdmin, MigrationsCtrl.migrateUserGame2);
api.get('/migrations/games/', md_auth.isAdmin, MigrationsCtrl.migrateGames);
api.get('/migrations/queue', md_auth.isAdmin, MigrationsCtrl.invalidateQueue);
api.get('/migrations/queue/dates', md_auth.isAdmin, MigrationsCtrl.migrateDatesQueue);
api.get('/migrations/queue/bools', md_auth.isAdmin, MigrationsCtrl.migrateBooleanQueue);
api.get('/migrations/queue/drop', md_auth.isAdmin, MigrationsCtrl.dropGamesQueue);
api.get('/migrations/tokens', md_auth.isAdmin, MigrationsCtrl.migrateTokens);

module.exports = api;
