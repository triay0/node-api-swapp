'use strict'
var express = require("express");
var TrophyCtrl = require('../controllers/trophy');

var api = express.Router();
var md_auth = require('../middlewares/authenticated');

api.post('/trophy', md_auth.isAuthenticated, TrophyCtrl.saveTrophy);
api.get('/trophy', md_auth.isAuthenticated, TrophyCtrl.getTrophies);

//Get Orders by user

module.exports = api;
