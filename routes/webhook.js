'use strict'
var express = require("express");
var StripeCtrl = require('../controllers/stripe');

var api = express.Router();
//var md_auth = require('../middlewares/authenticated');

api.post('/stripe', StripeCtrl.webhook);
api.get('/stripe', StripeCtrl.getActiveSubscriptions);

module.exports = api;
