'use strict'
var admin = require('firebase-admin');
var User = require('../models/user');
var UsersCtrl = require('../controllers/user');


var serviceAccount = require('../swappgames-9b257-firebase-adminsdk-1il7y-0488247459.json');

admin.initializeApp({
  credential: admin.credential.cert(serviceAccount),
  databaseURL: "https://swappgames-9b257.firebaseio.com"
});



function isAuthenticated(req, res, next) {
  if (!req.headers.authorization) {
    res.status(400).send('No token sended');
  }

  // User.findOne({
  //     id: 11349
  //   })
  //
  //   .exec(function(err, user) {
  //     if (err) return res.send(500, err.message);
  //     if (!user) {
  //       return res.send(404, 'El usuario no existe.');
  //     } else {
  //       req.user = user;
  //         return next();
  //     }
  //   });
  if (req.headers.authorization == "9821") {
    req.user = [];
    req.user.sub = 'admin';
    return next();
  } else {
//
//
  admin.auth().verifyIdToken(req.headers.authorization)
    .then((decodedIdToken) => {
      // console.log(req.headers.authorization);
      // console.log(decodedIdToken);
// ObjectId("5ca484ab41f8b70013736bdc")
      User.findOne({
          uid: decodedIdToken.uid
        })
        .exec(function(err, user) {
          if (err) return res.send(500, err.message);
          if (!user) {
            req.user = [];
            req.user.name = decodedIdToken.name;
            req.user.uid = decodedIdToken.uid;
            req.user.email = decodedIdToken.email;
            UsersCtrl.saveUser(req, res);

          } else {
             console.log('user');
            req.user = user;
            return next();
          }
        });
      //return next();
    }).catch((error) => {
      console.error('Error while verifying Firebase ID token:', error);
      res.status(403).send('Unauthorized\n' + error);
    });
  }
}

function isAdmin(req, res, next) {
  if (!req.headers.authorization) {
    res.status(400).send('No token sended');
  }

  if (req.headers.authorization == "9821") {
    req.user = [];
    req.user.sub = 'admin';
    return next();
  } else {
    res.status(403).send('Unauthorized\n');
  }
}

module.exports = {
  isAuthenticated,
  isAdmin
}
