var mongoose = require('mongoose'),
    Schema   = mongoose.Schema;

var OfficeSchema = new Schema({
  id:     { type: Number },
  name:    { type: String,  required: true },
  address:  { type: String,  required: true },
  zipcode:   { type: String },
  location:   { type: String },
  phone:   { type: String },
  scheduleMF:   { type: String },
  scheduleS:   { type: String },
  scheduleH:   { type: String },
  lat:   { type: Number },
  long:   { type: Number },
});

module.exports = mongoose.model('Office', OfficeSchema);
