var mongoose = require('mongoose'),
  Schema = mongoose.Schema;

var UserSchema = new Schema({

  //AUTH PROVIDER
  userId: {
    type: Number
  },
  uid: {
    type: String,
    required: true
  },
  name: {
    type: String,
    required: true
  },
  nick: {
    type: String
  },
  email_auth: {
    type: String,
    //required: true
  },
  phone: {
    type: String
  },
  stripe_id: {
    type: String
  },
  credit: {
    type: Number
  },
  idGame1: {
    type: String
  },
  idGame2: {
    type: String
  },
  idGame3: {
    type: String
  },
  gamei1: {
    type: Schema.ObjectId,
    ref: 'Game',

  },
  gamei2: {
    type: Schema.ObjectId,
    ref: 'Game',
  },
  gamei3: {
    type: Schema.ObjectId,
    ref: 'Game',

  },
  // postal1: {
  //   type: String,
  // },
  // postal2: {
  //   type: String,
  // },
  // province: {
  //   type: String,
  // },
  // zipcode: {
  //   type: String,
  // },
  // city: {
  //   type: String,
  // },
  address: {
    postal1: {
      type: String,
      default: ""
    },
    postal2: {
      type: String,
      default: ""
    },
    province: {
      type: String,
      default: ""
    },
    zipcode: {
      type: String,
      default: ""
    },
    city: {
      type: String,
      default: ""
    },
  },
  dni: {
    type: String
  },
  completeName: {
    type: String
  },
  createdAt: {
    type: String
  },
  games: [{
    type: Schema.Types.ObjectId,
    ref: 'Game'
  }],
  fcmTokens: [{
    type: Schema.Types.ObjectId,
    ref: 'Token'
  }],
  subscriptions: [{
    type: Schema.Types.ObjectId,
    ref: 'Subscription'
  }],
  subscription: {
    type: Schema.Types.ObjectId,
    ref: 'Subscription'
  },
  queue: [{
    type: Schema.Types.ObjectId,
    ref: 'QueueGame'
  }],
  orders: [{
    type: Schema.Types.ObjectId,
    ref: 'Order'
  }],

}, {
  usePushEach: true,
  versionKey: false
});

module.exports = mongoose.model('User', UserSchema);
