'use strict'
var mongoose = require('mongoose');
var Schema = mongoose.Schema;

var QueueGameSchema = new Schema({
  game_id: {
    type: String,
    //ref: 'Game'
  },
  user_id: {
    type: String,
    // type: Schema.ObjectId,
    // ref: 'User'
  },
  game: {
    type: Schema.ObjectId,
    ref: 'Game'
  },
  user: {
    type: Schema.ObjectId,
    ref: 'User'
  },
  deleted: {
    type: Boolean
  },
  requested: {
    type: Boolean
  },
  createdAt: {
    type: Date
  },
  date_added: {
    type: String
  },
  platform: {
    type: Number
  },

});

module.exports = mongoose.model('QueueGame', QueueGameSchema);
