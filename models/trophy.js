'use strict'
var mongoose = require('mongoose');
var Schema = mongoose.Schema;

var TrophySchema = new Schema({
  title: {
    type: String,
    required: true
  },
  icon: {
    type: String
  },
  visible:{
    type: Boolean
  },
  description:{
    type: String
  }
});

module.exports = mongoose.model('Trophy', TrophySchema);
