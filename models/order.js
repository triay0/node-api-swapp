'use strict'

var mongoose = require('mongoose');
var Schema = mongoose.Schema;

var OrderSchema = new Schema({
  id: {
    type: Number
  },
  user: {
    type: Schema.ObjectId,
    ref: 'User',
  //  required: true
  },
  name: {
    type: String,
    required: true
  },
  expectedDeliveryDate:{
    type:Date
  },
  phone: {
    type: Number,
    required: true
  },
  buy: {
    type: Boolean,
    //required: true
  },
  user_id: {
    type: String
  },
  game_id: {
    type: String
  },
  dni: {
    type: String
  },
  postal1: {
    type: String,
  },
  postal2: {
    type: String,
  },
  province: {
    type: String,
  },
  zipcode: {
    type: String,
  },
  city: {
    type: String,
  },
  address: {
    postOffice: {
      type: String
    },
    postal1: {
      type: String
    },
    number: {
      type: String
    },
    postal2: {
      type: String
    },
    province: {
      type: String
    },
    zipcode: {
      type: String
    },
    city: {
      type: String
    }
  },
  gameIn: [{
    type: Schema.ObjectId,
    ref: 'Game'
  }],
  gameOut: [{
    type: Schema.ObjectId,
    ref: 'Game'
  }],
  comment: {
    type: String
  },
  date:{ type: String},
  gi1: {
    type: Number
  },
  gi2: {
    type: Number
  },
  gi3: {
    type: Number
  },
  go1: {
    type: Number
  },
  go2: {
    type: Number
  },
  go3: {
    type: Number
  },
  gamei1: {
    type: Schema.ObjectId,
    ref: 'Game',

  },
  gamei2: {
    type: Schema.ObjectId,
    ref: 'Game',
  },
  gamei3: {
    type: Schema.ObjectId,
    ref: 'Game',

  },
  gameo1: {
    type: Schema.ObjectId,
    ref: 'Game',

  },
  gameo2: {
    type: Schema.ObjectId,
    ref: 'Game',

  },
  gameo3: {
    type: Schema.ObjectId,
    ref: 'Game',

  },
  sended: {
    type: Boolean
  },

  createdAt: {
    type: Date
  },
  platform: {
    type: Number,
    required: true,
    default: 1
  },
  tracking: {
    type: String
  },

  trackingIn: {
    type: String
  },
  trackingOut: {
    type: String
  },
  status: {
    type: Number,
    required: true,
    default: 0
  },
  statusFlutter: {
    type: Number,
    default: 1
  },
  statusMessage: {
    type: String,

  },
  timeZone: {
    type: String
  },
  from: {
    type: String
  }

});

module.exports = mongoose.model('Order', OrderSchema);
