'use strict'
var mongoose = require('mongoose');
var Schema = mongoose.Schema;

var SubscriptionSchema = new Schema({


  subscription_id: {
    type: String,
    required: true
  },
  user_id: {
    type: String
  },
  plan: {
    type: String,
    required: true
  },
  user: {
    type: Schema.ObjectId,
    ref: 'User'
  },
  status: {
    type: String,
    required: true
  },
  createdAt: {
    type: Date
  },
  updatedAt: {
    type: String
  },
  canceledAt: {
    type: String
  },
  current_period_end: {
    type: Date
  },
  current_period_end: {
    type: Date
  },
  date_return_games: {
    type: Date
  },
  platform: {
    type: String,
    // required: true
  },
  reason: {
    type: String
  },
  comment:{
    type: String
  }
});

module.exports = mongoose.model('Subscription', SubscriptionSchema);
