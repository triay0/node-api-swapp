'use strict'

var mongoose = require('mongoose');
var Schema = mongoose.Schema;

var ReviewSchema = new Schema({
  user3:  { type: String},// Schema.ObjectId, ref: 'User', required: true },
  game3:  { type:  String},//Schema.ObjectId, ref: 'Game', required: true },
  game:  { type: Schema.ObjectId, ref: 'Game', required: true },
  user:  { type: Schema.ObjectId, ref: 'User'},
  rating: { type: Number , required: true },
  comment: { type: String},
  createdAt:{ type: Date},
  platform: { type: Number, required: true},
});

module.exports = mongoose.model('Review', ReviewSchema);
