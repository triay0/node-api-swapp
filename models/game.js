var mongoose = require('mongoose'),
  Schema = mongoose.Schema;

var GameSchema = new Schema({
  id: {
    type: Number
  },
  stock: {
    type: Number
  },
  name: {
    type: String,
    required: true
  },
  released: {
    type: Boolean
  },
  cover: {
    type: String
  },
  screenshot: {
    type: String
  },
  booking: {
    type: Number,
    default: -1
  },
  genres: [{
    type: Schema.Types.ObjectId,
    ref: 'Genre'
  }],
  summary: {
    type: String
  },
  pegi: {
    type: Number
  },
  premium: {
    type: Boolean
  },
  psplus: {
    type: Boolean,
    default: false
  },
  sku_cex: {
    type: String
  },
  sku_game: {
    type: String
  },
  price_cex: {
    type: Number
  },
  price_game: {
    type: Number
  },
  reviews: [{
    type: Schema.Types.ObjectId,
    ref: 'Review'
  }],
  queue: [{
    type: Schema.Types.ObjectId,
    ref: 'QueueGame'
  }],
  queueSize: {
    type: Number
  },

}, {
  usePushEach: true,
 versionKey: false
});

module.exports = mongoose.model('Game', GameSchema);
