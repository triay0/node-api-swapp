var mongoose = require('mongoose'),
    Schema   = mongoose.Schema;

var GenreSchema = new Schema({
  name: { type: String,  required: true },
}, {
  usePushEach: true,
 versionKey: false
});

module.exports = mongoose.model('Genre', GenreSchema);
