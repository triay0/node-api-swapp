'use strict'
var mongoose = require('mongoose');
var Schema = mongoose.Schema;

var TokenSchema = new Schema({
  token: {
    type: String,
    required: true
  },
  platform: {
    type: Number,
    required: true
  },
  user: {
    type: Schema.ObjectId,
    ref: 'User'
  },
  user_id: {
    type: Number
  },
  createdAt:{ type: Date},
  valid: {
    type: Boolean
  }
});

module.exports = mongoose.model('Token', TokenSchema);
